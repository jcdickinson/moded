{
  description = "Dagwood Crates";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    flake-utils,
    rust-overlay,
    advisory-db,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      overlays = [(import rust-overlay)];
      pkgs = import nixpkgs {
        inherit system overlays;
      };

      inherit (pkgs) lib rust-bin;

      rustToolchain = rust-bin.fromRustupToolchainFile ./rust-toolchain;
      craneLib = (crane.mkLib pkgs).overrideToolchain rustToolchain;
      src = craneLib.cleanCargoSource ./.;

      nativeBuildInputs = with pkgs; [
        pkg-config
        clang_14
        llvmPackages_14.bintools
      ];

      buildInputs = with pkgs; [
        openssl
      ];

      cargoArtifacts = craneLib.buildDepsOnly {
        inherit src buildInputs nativeBuildInputs;
      };

      my-crate = craneLib.buildPackage {
        inherit cargoArtifacts src buildInputs nativeBuildInputs;
      };
    in rec {
      formatter = pkgs.alejandra;

      checks =
        {
          inherit my-crate;

          my-crate-clippy = craneLib.cargoClippy {
            inherit cargoArtifacts src buildInputs nativeBuildInputs;
            cargoClippyExtraArgs = "--all-targets -- --deny warnings";
          };

          my-crate-doc = craneLib.cargoDoc {
            inherit cargoArtifacts src buildInputs nativeBuildInputs;
          };

          my-crate-fmt = craneLib.cargoFmt {
            inherit src;
          };

          my-crate-audit = craneLib.cargoAudit {
            inherit src advisory-db;
          };

          my-crate-nextest = craneLib.cargoNextest {
            inherit cargoArtifacts src buildInputs nativeBuildInputs;
            partitions = 1;
            partitionType = "count";
          };
        }
        // lib.optionalAttrs (system == "x86_64-linux") {
          my-crate-coverage = craneLib.cargoTarpaulin {
            inherit cargoArtifacts src;
          };
        };

      packages.default = my-crate;

      apps.default = flake-utils.lib.mkApp {
        drv = my-crate;
      };

      devShells.default = pkgs.mkShell {
        inputsFrom = builtins.attrValues checks;

        nativeBuildInputs = with pkgs;
          [
            rustToolchain
            overmind
            just
            cargo-nextest
            cargo-llvm-cov
          ]
          ++ nativeBuildInputs
          ++ buildInputs;
      };
    });
}
