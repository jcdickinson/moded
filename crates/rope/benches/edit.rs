use criterion::{black_box, criterion_group, criterion_main, Criterion};
// Copyright 2017 The xi-editor Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
use moded_rope::rope::Rope;

fn build_triangle(n: usize) -> String {
    let mut s = String::new();
    let mut line = String::new();
    for _ in 0..n {
        s += &line;
        s += "\n";
        line += "a";
    }
    black_box(s)
}

fn build_short_lines(n: usize) -> String {
    let line = "match s.as_bytes()[minsplit - 1..splitpoint].iter().rposition(|&c| c == b'\n') {";
    let mut s = String::new();
    for _ in 0..n {
        s += line;
    }
    black_box(s)
}

fn build_few_big_lines(size: usize) -> String {
    let mut s = String::with_capacity(size * 10 + 20);
    for _ in 0..10 {
        for _ in 0..size {
            s += "a";
        }
        s += "\n";
    }
    black_box(s)
}

fn file_load_short_lines(c: &mut Criterion) {
    let text = build_short_lines(50_000);
    c.bench_function("file_load_short_lines", |b| {
        b.iter(|| {
            black_box(Rope::from(&text));
        })
    });
}

fn file_load_few_big_lines(c: &mut Criterion) {
    let text = build_few_big_lines(1_000_000);
    c.bench_function("file_load_few_big_lines", |b| {
        b.iter(|| {
            black_box(Rope::from(&text));
        });
    });
}

fn char_insertion_one_line_edit(c: &mut Criterion) {
    let mut text = Rope::from("b".repeat(100));
    let mut offset = 100;
    c.bench_function("char_insertion_one_line_edit", |b| {
        b.iter(|| {
            text.edit(offset..=offset, "a");
            offset += 1;
        })
    });
}

fn paste_into_line(c: &mut Criterion) {
    let mut text = Rope::from(build_short_lines(50_000));
    let insertion = "a".repeat(50);
    let mut offset = 100;
    c.bench_function("paste_into_line", |b| {
        b.iter(|| {
            text.edit(offset..=offset, &insertion);
            offset += 150;
        })
    });
}

fn insert_newline(c: &mut Criterion) {
    let mut text = Rope::from(build_few_big_lines(1_000_000));
    let mut offset = 1000;
    c.bench_function("insert_newline", |b| {
        b.iter(|| {
            text.edit(offset..=offset, "\n");
            offset += 1001;
        })
    });
}

fn overwrite_into_line(c: &mut Criterion) {
    let mut text = Rope::from(build_short_lines(50_000));
    let mut offset = 100;
    let insertion = "a".repeat(50);
    c.bench_function("overwrite_into_line", |b| {
        b.iter(|| {
            // TODO: if the method runs too quickly, this may generate a fault
            // since there's an upper limit to how many times this can run.
            text.edit(offset..=offset + 20, &insertion);
            offset += 30;
        })
    });
}

fn triangle_concat_inplace(c: &mut Criterion) {
    let mut text = Rope::from("");
    let insertion = build_triangle(3000);
    let insertion_len = insertion.len();
    let mut offset = 0;
    c.bench_function("triangle_concat_inplace", |b| {
        b.iter(|| {
            text.edit(offset..=offset, &insertion);
            offset += insertion_len;
        })
    });
}

fn real_world_editing(c: &mut Criterion) {
    c.bench_function("real_world_editing", |b| {
        b.iter(|| {
            let mut text = Rope::default();
            let mut cursor = 0;
            for i in 1..10_000 {
                let s = if i % 80 == 0 { "\n" } else { "a" };
                text.edit(cursor..cursor, s);
                if i % 123 == 0 {
                    // periodically do some deletes
                    text.edit(cursor - 5..cursor, "");
                }

                // periodically move cursor:
                cursor = match i {
                    1000 => 200,
                    2000 => 1800,
                    3000 => 1000,
                    4000 => text.len() - 1,
                    5000 => 404,
                    6000 => 4444,
                    7000 => 6990,
                    8000 => 6990,
                    9000 => 100,
                    n if n % 123 == 0 => cursor - 5, // the delete case
                    _ => cursor + 1,
                };
            }
        })
    });
}

criterion_group!(
    benches,
    file_load_short_lines,
    file_load_few_big_lines,
    char_insertion_one_line_edit,
    paste_into_line,
    insert_newline,
    overwrite_into_line,
    triangle_concat_inplace,
    real_world_editing
);
criterion_main!(benches);
