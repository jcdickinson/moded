use std::{
    fmt::Debug,
    io::Write,
    ops::{Deref, Index, Range, RangeFrom},
    os::unix::prelude::OpenOptionsExt,
    rc::Rc,
};

use once_cell::sync::Lazy;

use crate::util::Buffer;

static SEMI: &'static [u8] = b";";

const MAX_PARAMETER_VALUE: u16 = 9999;
static NUMBERS: Lazy<Vec<&'static [u8]>> = Lazy::new(|| {
    let mut result = Vec::with_capacity(MAX_PARAMETER_VALUE as usize);

    for i in 0..10000 {
        let b = Box::leak(Box::new(i.to_string()));
        result.push(b.as_bytes());
    }

    result
});

static PARAMETERS: Lazy<Vec<&mut [u8; 1]>> = Lazy::new(|| {
    let mut result = Vec::with_capacity(16);

    for i in 0..16u8 {
        let buf = Box::leak(Box::new([b'0' + i]));
        result.push(buf)
    }

    result
});

pub const VT_100: u16 = 1;
pub const VT_220: u16 = 62;
pub const VT_320: u16 = 63;
pub const VT_420: u16 = 64;
pub const MODE_SYNCHRONIZED_OUTPUT: u16 = 2026;

macro_rules! term_cap_names {
    ($($name: ident => $hex: literal),+) => {
        #[derive(Clone, PartialEq, Eq, Hash)]
        pub enum TermCapName {
            $($name),+,
            Unknown(Vec<u8>)
        }

        impl AsRef<[u8]> for TermCapName {
            fn as_ref(&self) -> &[u8] {
                match self {
                    $( Self::$name => $hex ),+,
                    Self::Unknown(v) => &v
                }
            }
        }

        impl From<&[u8]> for TermCapName {
            fn from(value: &[u8]) -> TermCapName {
                match value {
                    $( $hex => Self::$name ),+,
                    unknown => Self::Unknown(unknown.to_vec())
                }
            }
        }
    };
}

term_cap_names! {
    TerminalName => b"544E",
    Colors => b"636F6C6F7273",
    TrueColor => b"5463",
    StyledUnderlines => b"5375",
    FullKeyboard => b"66756C6C6B6264"
}

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum Parameter {
    Selective(u8),
    Numeric(u16),
}

impl From<u16> for Parameter {
    fn from(value: u16) -> Self {
        Self::Numeric(value)
    }
}

impl From<&[u8; 1]> for Parameter {
    fn from(value: &[u8; 1]) -> Self {
        Self::Selective(value[0])
    }
}

#[cfg(not(tarpaulin_include))]
impl Debug for Parameter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Selective(arg0) => f.write_fmt(format_args!("{}", *arg0 as char)),
            Self::Numeric(arg0) => f.write_fmt(format_args!("{}", *arg0)),
        }
    }
}

pub trait Sequence {
    fn write(&self, is_8bit: bool, io: &mut impl Write) -> Result<(), std::io::Error>;
    fn parse(buf: &mut Buffer) -> ParseResult;
}

#[derive(Clone, PartialEq, Eq)]
pub struct ControlSequence {
    is_dec: bool,
    parameters: [Parameter; 16],
    parameter_count: u8,
    intermediates: Vec<u8>,
    final_char: u8,
}

impl ControlSequence {
    pub fn new(
        is_dec: bool,
        parameters: impl AsRef<[Parameter]>,
        intermediates: impl AsRef<[u8]>,
        final_char: u8,
    ) -> Self {
        let pref = parameters.as_ref();
        debug_assert!(
            pref.len() <= 16,
            "CS only supports less than or 16 parameters"
        );

        let parameter_count = pref.len() as u8;
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0..pref.len()].copy_from_slice(pref);

        let intermediates = intermediates.as_ref().to_vec();

        Self {
            is_dec,
            parameters,
            parameter_count,
            intermediates,
            final_char,
        }
    }

    pub fn parameters(&self) -> &[Parameter] {
        &self.parameters[..(self.parameter_count as usize)]
    }

    pub fn is_dec(&self) -> bool {
        self.is_dec
    }

    pub fn intermediates(&self) -> &[u8] {
        self.intermediates.as_ref()
    }

    pub fn final_char(&self) -> u8 {
        self.final_char
    }
}

impl Sequence for ControlSequence {
    fn write(&self, is_8bit: bool, io: &mut impl Write) -> Result<(), std::io::Error> {
        if is_8bit {
            io.write_all(b"\x9B")?;
        } else {
            io.write_all(b"\x1B[")?;
        }

        if self.is_dec {
            io.write_all(b"?")?;
        }

        write_parameters(io, self.parameters())?;

        io.write_all(&self.intermediates)?;
        io.write_all(&[self.final_char; 1])?;

        io.flush()?;

        Ok(())
    }

    fn parse(buf: &mut Buffer) -> ParseResult {
        let mut i = match buf.as_slice() {
            [b'\x1B', b'[', ..] => 2,
            [b'\x1B', _, ..] => return ParseResult::with_ignore(1),
            [b'\x1B'] => return ParseResult::with_required(0, 1),
            [b'\x9B', ..] => 1,
            [_, b'\x9B', ..] | [_, b'\x1B', ..] => return ParseResult::with_ignore(1),
            _ => return ParseResult::with_ignore(1),
        };

        let is_dec = match buf[i..] {
            [b'?', ..] => {
                i += 1;
                true
            }
            [_, ..] => false,
            _ => return ParseResult::with_required(0, 1),
        };

        let mut parameters = [Parameter::Numeric(0); 16];
        let mut parameter_count = 0;

        parse_parameters(buf, &mut i, &mut parameters, &mut parameter_count);

        let (intermediates, final_char) = match parse_intermediates(buf, &mut i) {
            Some(v) => v,
            None => return ParseResult::with_required(0, 1),
        };

        buf.consume(i);
        ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence {
            is_dec,
            parameters,
            parameter_count,
            intermediates,
            final_char,
        }))
    }
}

#[cfg(not(tarpaulin_include))]
impl Debug for ControlSequence {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let intermediates = self
            .intermediates
            .iter()
            .map(|f| *f as char)
            .collect::<String>();
        f.debug_struct("ControlSequence")
            .field("is_dec", &self.is_dec)
            .field("parameters", &self.parameters())
            .field("intermediates", &intermediates)
            .field("final_char", &(self.final_char as char))
            .finish()
    }
}

#[derive(Clone, PartialEq, Eq)]
pub struct DeviceControlString {
    parameters: [Parameter; 16],
    parameter_count: u8,
    intermediates: Vec<u8>,
    final_char: u8,
    data_string: Vec<Vec<u8>>,
}

impl DeviceControlString {
    pub fn new<DI, DO, P, I>(
        parameters: P,
        intermediates: I,
        final_char: u8,
        data_string: DO,
    ) -> Self
    where
        DI: AsRef<[u8]>,
        DO: AsRef<[DI]>,
        P: AsRef<[Parameter]>,
        I: AsRef<[u8]>,
    {
        let pref = parameters.as_ref();
        debug_assert!(
            pref.len() <= 16,
            "CS only supports less than or 16 parameters"
        );

        let parameter_count = pref.len() as u8;
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0..pref.len()].copy_from_slice(pref);

        let intermediates = intermediates.as_ref().to_vec();
        let data_string = data_string
            .as_ref()
            .iter()
            .map(|v| v.as_ref().to_vec())
            .collect();

        Self {
            parameters,
            parameter_count,
            intermediates,
            final_char,
            data_string,
        }
    }

    pub fn parameters(&self) -> &[Parameter] {
        &self.parameters[..(self.parameter_count as usize)]
    }
}

impl Sequence for DeviceControlString {
    fn write(&self, is_8bit: bool, io: &mut impl Write) -> Result<(), std::io::Error> {
        if is_8bit {
            io.write_all(b"\x90")?;
        } else {
            io.write_all(b"\x1BP")?;
        }

        write_parameters(io, self.parameters())?;

        io.write_all(&self.intermediates)?;
        io.write_all(&[self.final_char; 1])?;

        let mut need_semi = false;
        for p in &self.data_string {
            if need_semi {
                io.write_all(SEMI)?;
            }
            need_semi = true;
            io.write_all(&p)?;
        }

        if is_8bit {
            io.write_all(b"\x9C")?;
        } else {
            io.write_all(b"\x1B\x5C")?;
        }

        io.flush()?;

        Ok(())
    }

    fn parse(buf: &mut Buffer) -> ParseResult {
        let mut i = match buf.as_slice() {
            [b'\x1B', b'P', ..] => 2,
            [b'\x1B', _, ..] => return ParseResult::with_ignore(1),
            [b'\x1B'] | [b'\x90'] => return ParseResult::with_required(0, 1),
            [b'\x90', ..] => 1,
            [_, b'\x90', ..] | [_, b'\x1B', ..] => return ParseResult::with_ignore(1),
            _ => return ParseResult::with_ignore(1),
        };

        let mut parameters = [Parameter::Numeric(0); 16];
        let mut parameter_count = 0;

        parse_parameters(buf, &mut i, &mut parameters, &mut parameter_count);

        let (intermediates, final_char) = match parse_intermediates(buf, &mut i) {
            Some(v) => v,
            None => return ParseResult::with_required(0, 1),
        };

        let data_string = match parse_terminated_parameters(buf, &mut i) {
            Some(v) => v,
            None => return ParseResult::with_required(0, 1),
        };

        buf.consume(i);
        ParseResult::with_result(EscapeSequence::DeviceControlString(DeviceControlString {
            parameters,
            parameter_count,
            intermediates,
            data_string,
            final_char,
        }))
    }
}

#[cfg(not(tarpaulin_include))]
impl Debug for DeviceControlString {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let intermediates = self
            .intermediates
            .iter()
            .map(|f| *f as char)
            .collect::<String>();
        let data_string: Vec<_> = self
            .data_string
            .iter()
            .map(|x| String::from_utf8_lossy(x))
            .collect();
        f.debug_struct("DeviceControlString")
            .field("parameters", &self.parameters())
            .field("intermediates", &intermediates)
            .field("final_char", &(self.final_char as char))
            .field("data_string", &data_string)
            .finish()
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OperatingSystemCommand {
    parameters: Vec<Vec<u8>>,
}

impl OperatingSystemCommand {
    pub fn new<DI, DO>(parameters: DO) -> Self
    where
        DI: AsRef<[u8]>,
        DO: AsRef<[DI]>,
    {
        let parameters = parameters
            .as_ref()
            .iter()
            .map(|v| v.as_ref().to_vec())
            .collect();

        Self { parameters }
    }
}

impl Sequence for OperatingSystemCommand {
    fn write(&self, is_8bit: bool, io: &mut impl Write) -> Result<(), std::io::Error> {
        if is_8bit {
            io.write_all(b"\x9D")?;
        } else {
            io.write_all(b"\x1B]")?;
        }

        let mut need_semi = false;
        for p in &self.parameters {
            if need_semi {
                io.write_all(SEMI)?;
            }
            need_semi = true;
            io.write_all(&p)?;
        }

        if is_8bit {
            io.write_all(b"\x9C")?;
        } else {
            io.write_all(b"\x1B\x5C")?;
        }

        io.flush()?;

        Ok(())
    }

    fn parse(buf: &mut Buffer) -> ParseResult {
        let mut i = match buf.as_slice() {
            [b'\x1B', b']', ..] => 2,
            [b'\x1B', _, ..] => return ParseResult::with_ignore(1),
            [b'\x1B'] => return ParseResult::with_required(0, 1),
            [b'\x9D', ..] => 1,
            [_, b'\x9D', ..] | [_, b'\x1B', ..] => return ParseResult::with_ignore(1),
            _ => return ParseResult::with_ignore(1),
        };

        let parameters = match parse_terminated_parameters(buf, &mut i) {
            Some(v) => v,
            None => return ParseResult::with_required(0, 1),
        };

        buf.consume(i);
        ParseResult::with_result(EscapeSequence::OperatingSystemCommand(
            OperatingSystemCommand { parameters },
        ))
    }
}

#[inline]
fn write_parameters(io: &mut impl Write, p: &[Parameter]) -> Result<(), std::io::Error> {
    let mut need_semi = false;
    for p in p.iter() {
        if need_semi {
            io.write_all(SEMI)?;
        }
        need_semi = true;
        match p {
            Parameter::Selective(s) => io.write_all(PARAMETERS[(*s - b'0') as usize])?,
            Parameter::Numeric(s) => {
                io.write_all(NUMBERS[*s.min(&MAX_PARAMETER_VALUE) as usize])?
            }
        }
    }
    Ok(())
}

#[inline]
fn parse_parameters(
    buf: &mut Buffer,
    i: &mut usize,
    parameters: &mut [Parameter; 16],
    parameter_count: &mut u8,
) {
    let mut has_value = false;
    'parameters: while *i < buf.len() {
        match buf[*i] {
            b'@'..=b'~' | b' '..=b'/' => {
                break;
            }
            b';' => {
                // An empty parameter is equivalent to 0
                if !has_value {
                    parameters[*parameter_count as usize] = Parameter::Numeric(0);
                    *parameter_count += 1;
                }
                has_value = false;
            }
            b'0'..=b'9' => {
                let mut val = 0u16;
                while *i < buf.len() {
                    match buf[*i] {
                        b'0'..=b'9' => {
                            val = (val * 10) + (buf[*i] - b'0') as u16;
                        }
                        o => {
                            parameters[*parameter_count as usize] = Parameter::Numeric(val);
                            *parameter_count += 1;
                            val = 0;
                            if o != b';' {
                                continue 'parameters;
                            }
                        }
                    }
                    *i += 1;
                }
            }
            b':'..=b'?' => {
                parameters[*parameter_count as usize] = Parameter::Selective(buf[*i]);
                *parameter_count += 1;
                has_value = true;
            }
            _ => {}
        }
        *i += 1;
    }
}

#[inline]
fn parse_intermediates(buf: &mut Buffer, i: &mut usize) -> Option<(Vec<u8>, u8)> {
    let start = *i;
    loop {
        if *i >= buf.len() {
            break None;
        }
        match buf[*i] {
            b'@'..=b'~' => {
                let intermediates = buf[start..*i].to_vec();
                let final_char = buf[*i];
                *i += 1;
                break Some((intermediates, final_char));
            }
            _ => {}
        }
        *i += 1;
    }
}

#[inline]
fn parse_terminated_parameters(buf: &mut Buffer, i: &mut usize) -> Option<Vec<Vec<u8>>> {
    let mut start = *i;
    let mut parameters = Vec::new();
    loop {
        match buf[*i..] {
            [b'\x9C', ..] | [b'\x1B', b'\x5C', ..] => {
                if parameters.len() != 0 || *i > start {
                    parameters.push(buf[start..*i].to_vec());
                }
                if buf[*i] == b'\x9C' {
                    *i += 1;
                } else {
                    *i += 2;
                }
                break;
            }
            [b';', ..] => {
                parameters.push(buf[start..*i].to_vec());
                start = *i + 1;
            }
            [] => return None,
            _ => {}
        }
        *i += 1;
    }
    Some(parameters)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum EscapeSequence {
    ControlSequence(ControlSequence),
    DeviceControlString(DeviceControlString),
    OperatingSystemCommand(OperatingSystemCommand),
}

impl From<ControlSequence> for EscapeSequence {
    fn from(value: ControlSequence) -> Self {
        Self::ControlSequence(value)
    }
}

impl From<DeviceControlString> for EscapeSequence {
    fn from(value: DeviceControlString) -> Self {
        Self::DeviceControlString(value)
    }
}

impl From<OperatingSystemCommand> for EscapeSequence {
    fn from(value: OperatingSystemCommand) -> Self {
        Self::OperatingSystemCommand(value)
    }
}

impl Sequence for EscapeSequence {
    fn write(&self, is_8bit: bool, io: &mut impl Write) -> Result<(), std::io::Error> {
        match self {
            EscapeSequence::ControlSequence(r) => r.write(is_8bit, io),
            EscapeSequence::DeviceControlString(r) => r.write(is_8bit, io),
            EscapeSequence::OperatingSystemCommand(r) => r.write(is_8bit, io),
        }
    }

    fn parse(buf: &mut Buffer) -> ParseResult {
        for i in 0..buf.len() {
            match buf[i..] {
                [b'\x9B', ..] | [b'\x1B', b'[', ..] => {
                    if i != 0 {
                        return ParseResult::with_ignore(i);
                    }

                    return ControlSequence::parse(buf);
                }
                [b'\x9D', ..] | [b'\x1B', b']', ..] => {
                    if i != 0 {
                        return ParseResult::with_ignore(i);
                    }

                    return OperatingSystemCommand::parse(buf);
                }
                [b'\x90', ..] | [b'\x1B', b'P', ..] => {
                    if i != 0 {
                        return ParseResult::with_ignore(i);
                    }

                    return DeviceControlString::parse(buf);
                }
                [b'\x1B', _, ..] => return ParseResult::with_ignore(1),
                [b'\x1B'] => return ParseResult::with_required(i, 1),
                [b'\x03', ..] => {
                    debug_assert!(false, "CTRL+C pressed");
                }
                _ => {}
            }
        }

        ParseResult::with_ignore(buf.len())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ParseResult {
    result: Option<EscapeSequence>,
    /// The given number of bytes must be ignored prior to attempting to parse again.
    ignore: usize,
    /// Any amount of new bytes are required.
    required: Option<usize>,
}

impl ParseResult {
    pub fn with_ignore(ignore: usize) -> Self {
        Self {
            required: None,
            result: None,
            ignore,
        }
    }

    pub fn with_required(ignore: usize, required: usize) -> Self {
        Self {
            required: Some(required),
            result: None,
            ignore,
        }
    }

    pub fn with_result(result: EscapeSequence) -> Self {
        Self {
            required: None,
            result: Some(result),
            ignore: 0,
        }
    }

    pub fn result(&self) -> Option<&EscapeSequence> {
        self.result.as_ref()
    }

    pub fn ignore(&self) -> usize {
        self.ignore
    }

    pub fn required(&self) -> Option<usize> {
        self.required
    }
}

pub fn cs(
    parameters: impl AsRef<[Parameter]>,
    intermediates: impl AsRef<[u8]>,
    final_char: u8,
) -> EscapeSequence {
    EscapeSequence::ControlSequence(ControlSequence::new(
        false,
        parameters,
        intermediates,
        final_char,
    ))
}

pub fn dec(
    parameters: impl AsRef<[Parameter]>,
    intermediates: impl AsRef<[u8]>,
    final_char: u8,
) -> EscapeSequence {
    EscapeSequence::ControlSequence(ControlSequence::new(
        true,
        parameters,
        intermediates,
        final_char,
    ))
}

pub fn dcs<DI, DO, P, I>(
    parameters: P,
    intermediates: I,
    final_char: u8,
    data_string: DO,
) -> EscapeSequence
where
    DI: AsRef<[u8]>,
    DO: AsRef<[DI]>,
    P: AsRef<[Parameter]>,
    I: AsRef<[u8]>,
{
    EscapeSequence::DeviceControlString(DeviceControlString::new(
        parameters,
        intermediates,
        final_char,
        data_string,
    ))
}

pub fn osc<DI, DO>(parameters: DO) -> EscapeSequence
where
    DI: AsRef<[u8]>,
    DO: AsRef<[DI]>,
{
    EscapeSequence::OperatingSystemCommand(OperatingSystemCommand::new(parameters))
}

pub fn request_dec_mode(mode: u16) -> EscapeSequence {
    dec(&[mode.into()], b"$", b'p')
}

pub fn request_primary_device_attributes() -> EscapeSequence {
    cs(&[0.into()], &[], b'c')
}

pub fn select_conformance_level(level: u16) -> EscapeSequence {
    let level = level + 60;
    cs(&[level.into()], b"\"", b'p')
}

pub fn alternate_screen(enable: bool) -> EscapeSequence {
    dec(&[1049.into()], &[], if enable { b'h' } else { b'l' })
}

pub fn synchronized_output(enable: bool) -> EscapeSequence {
    dec(&[2026.into()], &[], if enable { b'h' } else { b'l' })
}

pub fn get_term_cap(caps: impl AsRef<[TermCapName]>) -> EscapeSequence {
    EscapeSequence::DeviceControlString(DeviceControlString::new(&[], b"+", b'q', caps))
}

pub fn set_style(fg: u16) -> EscapeSequence {
    cs(&[fg.into()], b"", b'm')
}

pub fn reset_style() -> EscapeSequence {
    cs(&[0.into()], b"", b'm')
}

#[cfg(test)]
mod tests {
    use super::*;

    fn buffer(mut d: &[u8]) -> Buffer {
        let mut r = Buffer::default();
        r.ensure_capacity(d.len());
        r.read_from(&mut d).expect("buffer read");
        r
    }

    #[test]
    fn esc_parse_ignored() {
        let mut buf = buffer(b"\x1BQ");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(result, ParseResult::with_ignore(1));
    }

    #[test]
    fn esc_parse_incomplete() {
        let mut buf = buffer(b"ABC\x1B");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(result, ParseResult::with_required(3, 1));
    }

    #[test]
    fn esc_parse_ignore_all() {
        let mut buf = buffer(b"ABC");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(result, ParseResult::with_ignore(3));
    }

    #[test]
    fn esc_parse_dec_7bit() {
        let mut buf = buffer(b"\x1B[?:;123;9999$#%Z");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                true,
                &[b":".into(), 123.into(), 9999.into()],
                b"$#%",
                b'Z'
            )))
        );
    }

    #[test]
    fn esc_write_dec_7bit() {
        let mut buf = Vec::new();
        let result = EscapeSequence::ControlSequence(ControlSequence::new(
            true,
            &[b":".into(), 123.into(), 10000.into()],
            b"$@",
            b't',
        ));
        result.write(false, &mut buf).unwrap();

        assert_eq!(b"\x1B[?:;123;9999$@t", buf.as_slice());
    }

    #[test]
    fn esc_write_cs_8bit() {
        let mut buf = Vec::new();
        let result = EscapeSequence::ControlSequence(ControlSequence::new(
            false,
            &[b":".into(), 123.into(), 10000.into()],
            b"$@",
            b't',
        ));
        result.write(true, &mut buf).unwrap();

        assert_eq!(b"\x9B:;123;9999$@t", buf.as_slice());
    }

    #[test]
    fn esc_parse_cs_8bit() {
        let mut buf = buffer(b"\x9B:;123;9999$#%Z");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                false,
                &[b":".into(), 123.into(), 9999.into()],
                b"$#%",
                b'Z'
            )))
        );
    }

    #[test]
    fn esc_parse_cs_ignored() {
        let mut buf = buffer(b"ABC\x9B:;123;9999$#%Z");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(result, ParseResult::with_ignore(3));
    }

    #[test]
    fn esc_parse_dcs_7bit() {
        let mut buf = buffer(b"\x1BP:;123;9999$#%Zfoo;bar\x1B\x5C");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::DeviceControlString(
                DeviceControlString::new(
                    &[b":".into(), 123.into(), 9999.into()],
                    b"$#%",
                    b'Z',
                    &[b"foo", b"bar"]
                )
            ))
        );
    }

    #[test]
    fn esc_parse_dcs_8bit() {
        let mut buf = buffer(b"\x90:;123;9999$#%Zfoo;bar\x1B\x5C");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::DeviceControlString(
                DeviceControlString::new(
                    &[b":".into(), 123.into(), 9999.into()],
                    b"$#%",
                    b'Z',
                    &[b"foo", b"bar"]
                )
            ))
        );
    }

    #[test]
    fn esc_parse_dcs_ignored() {
        let mut buf = buffer(b"ABC\x90:;123;9999$#%Z");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(result, ParseResult::with_ignore(3));
    }

    #[test]
    fn esc_write_osc_7bit() {
        let mut buf = Vec::new();
        let result = EscapeSequence::OperatingSystemCommand(OperatingSystemCommand::new(&[
            b"100", b"foo", b"bar",
        ]));
        result.write(false, &mut buf).unwrap();

        assert_eq!(b"\x1B]100;foo;bar\x1B\x5C", buf.as_slice());
    }

    #[test]
    fn esc_write_osc_8bit() {
        let mut buf = Vec::new();
        let result = EscapeSequence::OperatingSystemCommand(OperatingSystemCommand::new(&[
            b"100", b"foo", b"bar",
        ]));
        result.write(true, &mut buf).unwrap();

        assert_eq!(b"\x9D100;foo;bar\x9C", buf.as_slice());
    }

    #[test]
    fn esc_parse_osc_7bit() {
        let mut buf = buffer(b"\x1B]100;foo;bar\x1B\x5C");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::OperatingSystemCommand(
                OperatingSystemCommand::new(&[b"100", b"foo", b"bar"])
            ))
        );
    }

    #[test]
    fn esc_parse_osc_8bit() {
        let mut buf = buffer(b"\x9D100;foo;bar\x9C");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::OperatingSystemCommand(
                OperatingSystemCommand::new(&[b"100", b"foo", b"bar"])
            ))
        );
    }

    #[test]
    fn esc_parse_osc_ignored() {
        let mut buf = buffer(b"ABC\x9D:;123;9999$#%Z");
        let result = EscapeSequence::parse(&mut buf);
        assert_eq!(result, ParseResult::with_ignore(3));
    }

    #[test]
    fn cs_new() {
        let result =
            ControlSequence::new(false, &[b":".into(), 123.into(), 10000.into()], b"$@", b't');
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0] = b":".into();
        parameters[1] = 123.into();
        parameters[2] = 10000.into();
        assert_eq!(
            result,
            ControlSequence {
                is_dec: false,
                parameters,
                parameter_count: 3,
                intermediates: b"$@".to_vec(),
                final_char: b't'
            }
        );
        assert_eq!(false, result.is_dec());
        assert_eq!(b"$@", result.intermediates());
        assert_eq!(b't', result.final_char());
    }

    #[test]
    fn dec_new() {
        let result =
            ControlSequence::new(true, &[b":".into(), 123.into(), 10000.into()], b"$@", b't');
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0] = b":".into();
        parameters[1] = 123.into();
        parameters[2] = 10000.into();
        assert_eq!(
            result,
            ControlSequence {
                is_dec: true,
                parameters,
                parameter_count: 3,
                intermediates: b"$@".to_vec(),
                final_char: b't'
            }
        );
        assert_eq!(true, result.is_dec());
        assert_eq!(b"$@", result.intermediates());
        assert_eq!(b't', result.final_char());
    }

    #[test]
    fn dec_write_7bit() {
        let mut buf = Vec::new();
        let result =
            ControlSequence::new(true, &[b":".into(), 123.into(), 10000.into()], b"$@", b't');
        result.write(false, &mut buf).unwrap();

        assert_eq!(b"\x1B[?:;123;9999$@t", buf.as_slice());
    }

    #[test]
    fn cs_write_8bit() {
        let mut buf = Vec::new();
        let result =
            ControlSequence::new(false, &[b":".into(), 123.into(), 10000.into()], b"$@", b't');
        result.write(true, &mut buf).unwrap();

        assert_eq!(b"\x9B:;123;9999$@t", buf.as_slice());
    }

    #[test]
    fn dec_parse_7bit() {
        let mut buf = buffer(b"\x1B[?:;123;9999$#%Z");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                true,
                &[b":".into(), 123.into(), 9999.into()],
                b"$#%",
                b'Z'
            )))
        );
    }

    #[test]
    fn dec_parse_8bit() {
        let mut buf = buffer(b"\x9B?:;123;9999$#%Z");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                true,
                &[b":".into(), 123.into(), 9999.into()],
                b"$#%",
                b'Z'
            )))
        );
    }

    #[test]
    fn cs_parse_7bit() {
        let mut buf = buffer(b"\x1B[:;123;9999$#%Z");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                false,
                &[b":".into(), 123.into(), 9999.into()],
                b"$#%",
                b'Z'
            )))
        );
    }

    #[test]
    fn cs_parse_8bit() {
        let mut buf = buffer(b"\x9B:;123;9999$#%Z");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                false,
                &[b":".into(), 123.into(), 9999.into()],
                b"$#%",
                b'Z'
            )))
        );
    }

    #[test]
    fn cs_parse_parameters_no_intermediates() {
        let mut buf = buffer(b"\x9B:;123;9999@");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                false,
                &[b":".into(), 123.into(), 9999.into()],
                b"",
                b'@'
            )))
        );
    }

    #[test]
    fn cs_parse_parameters_empty_semi() {
        let mut buf = buffer(b"\x9B;123;9999@");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                false,
                &[0.into(), 123.into(), 9999.into()],
                b"",
                b'@'
            )))
        );
    }

    #[test]
    fn cs_parse_parameters_end_thing() {
        let mut buf = buffer(b"\x9B;123;9999:@");
        let result = ControlSequence::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::ControlSequence(ControlSequence::new(
                false,
                &[0.into(), 123.into(), 9999.into(), b":".into()],
                b"",
                b'@'
            )))
        );
    }

    #[test]
    fn cs_parse_ignored() {
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"\x1B")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"\x9B")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"Z\x1B")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"Z\x9B")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"\x1BQ")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"A")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            ControlSequence::parse(&mut buffer(b"\x1B[0$")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
    }

    #[test]
    fn dcs_new() {
        let result = DeviceControlString::new(
            &[b":".into(), 123.into(), 10000.into()],
            b"$@",
            b't',
            &[b"foo", b"bar"],
        );
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0] = b":".into();
        parameters[1] = 123.into();
        parameters[2] = 10000.into();
        assert_eq!(
            result,
            DeviceControlString {
                parameters,
                parameter_count: 3,
                intermediates: b"$@".to_vec(),
                final_char: b't',
                data_string: vec![b"foo".to_vec(), b"bar".to_vec()]
            }
        )
    }

    #[test]
    fn dcs_write_7bit() {
        let mut buf = Vec::new();
        let result = DeviceControlString::new(
            &[b":".into(), 123.into(), 10000.into()],
            b"$@",
            b't',
            &[b"foo", b"bar"],
        );
        result.write(false, &mut buf).unwrap();

        assert_eq!(b"\x1BP:;123;9999$@tfoo;bar\x1B\x5C", buf.as_slice());
    }

    #[test]
    fn dcs_write_7bit_get_term_cap() {
        let mut buf = Vec::new();
        let result = get_term_cap(&[TermCapName::Colors]);
        result.write(false, &mut buf).unwrap();
        assert_eq!(b"\x1BP+q636F6C6F7273\x1B\\", buf.as_slice());
    }

    #[test]
    fn dcs_write_8bit() {
        let mut buf = Vec::new();
        let result = DeviceControlString::new(
            &[b":".into(), 123.into(), 10000.into()],
            b"$@",
            b't',
            &[b"foo", b"bar"],
        );
        result.write(true, &mut buf).unwrap();

        assert_eq!(b"\x90:;123;9999$@tfoo;bar\x9C", buf.as_slice());
    }

    #[test]
    fn dcs_parse_7bit() {
        let mut buf = buffer(b"\x1BP:;123;9999$#%Zfoo;bar\x1B\x5C");
        let result = DeviceControlString::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::DeviceControlString(
                DeviceControlString::new(
                    &[b":".into(), 123.into(), 9999.into()],
                    b"$#%",
                    b'Z',
                    &[b"foo", b"bar"]
                )
            ))
        );
    }

    #[test]
    fn dcs_parse_8bit() {
        let mut buf = buffer(b"\x90:;123;9999$#%Zfoo;bar\x9C");
        let result = DeviceControlString::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::DeviceControlString(
                DeviceControlString::new(
                    &[b":".into(), 123.into(), 9999.into()],
                    b"$#%",
                    b'Z',
                    &[b"foo", b"bar"]
                )
            ))
        );
    }

    #[test]
    fn dcs_parse_ignored() {
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"\x1B")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"\x90")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"Z\x1B")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"Z\x90")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"\x1BQ")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"A")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"\x1BP0$")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            DeviceControlString::parse(&mut buffer(b"\x1BP0$m")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
    }

    #[test]
    fn osc_new() {
        let result = OperatingSystemCommand::new(&[b"100", b"foo", b"bar"]);
        assert_eq!(
            result,
            OperatingSystemCommand {
                parameters: vec![b"100".to_vec(), b"foo".to_vec(), b"bar".to_vec()]
            }
        )
    }

    #[test]
    fn osc_write_7bit() {
        let mut buf = Vec::new();
        let result = OperatingSystemCommand::new(&[b"100", b"foo", b"bar"]);
        result.write(false, &mut buf).unwrap();

        assert_eq!(b"\x1B]100;foo;bar\x1B\x5C", buf.as_slice());
    }

    #[test]
    fn osc_write_8bit() {
        let mut buf = Vec::new();
        let result = OperatingSystemCommand::new(&[b"100", b"foo", b"bar"]);
        result.write(true, &mut buf).unwrap();

        assert_eq!(b"\x9D100;foo;bar\x9C", buf.as_slice());
    }

    #[test]
    fn osc_parse_7bit() {
        let mut buf = buffer(b"\x1B]100;foo;bar\x1B\x5C");
        let result = OperatingSystemCommand::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::OperatingSystemCommand(
                OperatingSystemCommand::new(&[b"100", b"foo", b"bar"])
            ))
        );
    }

    #[test]
    fn osc_parse_8bit() {
        let mut buf = buffer(b"\x9D100;foo;bar\x9C");
        let result = OperatingSystemCommand::parse(&mut buf);
        assert_eq!(
            result,
            ParseResult::with_result(EscapeSequence::OperatingSystemCommand(
                OperatingSystemCommand::new(&[b"100", b"foo", b"bar"])
            ))
        );
    }

    #[test]
    fn osc_parse_ignored() {
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"\x1B")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"\x9D")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"Z\x1B")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"Z\x9D")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"\x1BQ")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"A")),
            ParseResult { ignore: 1, .. }
        ));
        assert!(matches!(
            OperatingSystemCommand::parse(&mut buffer(b"\x1B]0")),
            ParseResult { required: Some(x), .. } if x >= 1
        ));
    }

    #[test]
    fn factory_cs() {
        let result = cs(&[b":".into(), 123.into(), 10000.into()], b"$@", b't');
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0] = b":".into();
        parameters[1] = 123.into();
        parameters[2] = 10000.into();
        assert_eq!(
            result,
            ControlSequence {
                is_dec: false,
                parameters,
                parameter_count: 3,
                intermediates: b"$@".to_vec(),
                final_char: b't'
            }
            .into()
        );
    }

    #[test]
    fn factory_dec() {
        let result = dec(&[b":".into(), 123.into(), 10000.into()], b"$@", b't');
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0] = b":".into();
        parameters[1] = 123.into();
        parameters[2] = 10000.into();
        assert_eq!(
            result,
            ControlSequence {
                is_dec: true,
                parameters,
                parameter_count: 3,
                intermediates: b"$@".to_vec(),
                final_char: b't'
            }
            .into()
        );
    }

    #[test]
    fn factory_osc() {
        let result = osc(&[b"100", b"foo", b"bar"]);
        assert_eq!(
            result,
            OperatingSystemCommand {
                parameters: vec![b"100".to_vec(), b"foo".to_vec(), b"bar".to_vec()]
            }
            .into()
        )
    }

    #[test]
    fn factory_dcs() {
        let result = dcs(
            &[b":".into(), 123.into(), 10000.into()],
            b"$@",
            b't',
            &[b"foo", b"bar"],
        );
        let mut parameters = [Parameter::Numeric(0); 16];
        parameters[0] = b":".into();
        parameters[1] = 123.into();
        parameters[2] = 10000.into();
        assert_eq!(
            result,
            DeviceControlString {
                parameters,
                parameter_count: 3,
                intermediates: b"$@".to_vec(),
                final_char: b't',
                data_string: vec![b"foo".to_vec(), b"bar".to_vec()]
            }
            .into()
        )
    }

    #[test]
    fn factory_request_dec_mode() {
        let result = request_dec_mode(101);
        assert_eq!(result, dec(&[101.into()], b"$", b'p'))
    }

    #[test]
    fn factory_request_primary_device_attributes() {
        let result = request_primary_device_attributes();
        assert_eq!(result, cs(&[0.into()], b"", b'c'))
    }

    #[test]
    fn factory_select_conformance_level() {
        let result = select_conformance_level(3);
        assert_eq!(result, cs(&[63.into()], b"\"", b'p'))
    }

    #[test]
    fn factory_alternate_screen_enable() {
        let result = alternate_screen(true);
        assert_eq!(result, dec(&[1049.into()], b"", b'h'))
    }

    #[test]
    fn factory_alternate_screen_disable() {
        let result = alternate_screen(false);
        assert_eq!(result, dec(&[1049.into()], b"", b'l'))
    }

    #[test]
    fn factory_synchronized_output_enable() {
        let result = synchronized_output(true);
        assert_eq!(result, dec(&[2026.into()], b"", b'h'))
    }

    #[test]
    fn factory_synchronized_output_disable() {
        let result = synchronized_output(false);
        assert_eq!(result, dec(&[2026.into()], b"", b'l'))
    }
}
