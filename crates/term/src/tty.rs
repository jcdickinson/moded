use std::{
    borrow::Borrow,
    collections::HashSet,
    fs::File,
    io::{BufWriter, Read, Write},
    ops::{Deref, DerefMut},
    os::fd::{AsRawFd, FromRawFd, IntoRawFd},
    sync::{
        atomic::{AtomicBool, AtomicU16},
        Arc,
    },
    time::Duration,
};

use flume::{Receiver, Sender};
use parking_lot::{Mutex, RwLock, RwLockUpgradableReadGuard};
use termios::Termios;

use crate::{
    util::{Buffer, FileDescriptor},
    vt::{
        DecMode, DecModeRequest, DecModeReset, DecModeResponse, DecModeSet,
        DeviceAttributesResponse, Message, MessageRouter, MessageWriter, OperatingStatusRequest,
        OperatingStatusResponse, Parser, PrimaryDeviceAttribute, PrimaryDeviceAttributesRequest,
        Print, Scanner, SelectConformanceLevel, Version, MAX_PARAMETERS,
    },
};

use super::{Terminal, TerminalRenderer};

pub(crate) fn open() -> Result<File, std::io::Error> {
    File::options().read(true).write(true).open("/dev/tty")
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
enum Capability {
    SynchronizedOutput,
    AlternateScreen,
    SaveCursor,
}

#[derive(Clone)]
struct TtyState {
    caps: Arc<RwLock<HashSet<Capability>>>,
    attributes: Arc<RwLock<HashSet<PrimaryDeviceAttribute>>>,
    signal: Sender<Result<(), std::io::Error>>,
    is_8bit: Arc<AtomicBool>,
    version: Arc<AtomicU16>,
}

impl TtyState {
    pub fn new(signal: Sender<Result<(), std::io::Error>>) -> Self {
        Self {
            caps: Arc::new(RwLock::new(HashSet::new())),
            attributes: Arc::new(RwLock::new(HashSet::new())),
            signal,
            is_8bit: Arc::new(AtomicBool::new(false)),
            version: Arc::new(AtomicU16::new(Version::Vt100.into())),
        }
    }

    fn create_router(&self) -> MessageRouter {
        let mut router = MessageRouter::new();
        let mut cloned = self.clone();
        router.register(move |m| cloned.dec_mode_response(m));
        let mut cloned = self.clone();
        router.register(move |m| cloned.device_attributes_response(m));
        let mut cloned = self.clone();
        router.register(move |m| cloned.operating_status_response(m));
        router
    }

    fn operating_status_response(&mut self, _message: OperatingStatusResponse) -> Result<(), ()> {
        self.signal.borrow().send(Ok(())).unwrap();
        Ok(())
    }

    fn device_attributes_response(&mut self, message: DeviceAttributesResponse) -> Result<(), ()> {
        self.version
            .store(message.version.into(), std::sync::atomic::Ordering::Release);
        self.set_attributes(message.deref());
        Ok(())
    }

    fn dec_mode_response(&mut self, message: DecModeResponse) -> Result<(), ()> {
        use crate::vt::ModeSetting;
        match (message.mode, message.setting) {
            (_, ModeSetting::NotRecognized) => {}
            (DecMode::SynchronizedOutput, _) => self.set_cap(Capability::SynchronizedOutput),
            (DecMode::AlternateScreen, _) => self.set_cap(Capability::AlternateScreen),
            (DecMode::SaveCursor, _) => self.set_cap(Capability::SaveCursor),
            u => todo!("{:?}", u),
        }
        Ok(())
    }

    fn set_8bit(&self) {
        self.is_8bit
            .store(true, std::sync::atomic::Ordering::Release)
    }

    fn version(&self) -> Version {
        self.version
            .load(std::sync::atomic::Ordering::Acquire)
            .into()
    }

    fn is_8bit(&self) -> bool {
        self.is_8bit.load(std::sync::atomic::Ordering::Acquire)
    }

    fn set_cap(&self, capability: Capability) {
        let lock = self.caps.upgradable_read();
        if !lock.contains(&capability) {
            let mut lock = RwLockUpgradableReadGuard::<'_, _>::upgrade(lock);
            lock.insert(capability);
        }
    }

    fn set_attributes(&self, capabilities: &HashSet<PrimaryDeviceAttribute>) {
        let lock = self.attributes.upgradable_read();
        if !lock.is_superset(&capabilities) {
            let mut lock = RwLockUpgradableReadGuard::<'_, _>::upgrade(lock);
            for v in capabilities {
                lock.insert(*v);
            }
        }
    }

    pub fn supports(&self, capability: Capability) -> bool {
        let lock = self.caps.read();
        lock.contains(&capability)
    }
}

pub struct Tty<W: Write + FromRawFd = File> {
    writer: Mutex<BufWriter<W>>,
    is_alternate: bool,
    fd: Option<FileDescriptor<W>>,
    original: Option<Termios>,
    state: TtyState,
    receiver: Receiver<Result<(), std::io::Error>>,
    // https://docs.rs/signal-hook/latest/signal_hook/
}

impl<W: Write + FromRawFd> Drop for Tty<W> {
    fn drop(&mut self) {
        let undo_alternate_result = if self.is_alternate {
            DecModeReset::single(DecMode::AlternateScreen).write_to(self)
        } else {
            Ok(())
        };

        let undo_raw_result = match (self.fd.take(), self.original.take()) {
            (Some(fd), Some(original)) => {
                fd.use_fd(|fd| termios::tcsetattr(fd, termios::TCSANOW, &original))
            }
            _ => Ok(()),
        };

        crate::debug_assert_ok!(undo_alternate_result);
        crate::debug_assert_ok!(undo_raw_result);
    }
}

impl Terminal for Tty<File> {
    type Renderer<'a> = MioTtyRenderer<'a>
    where
        Self: 'a;

    type Error = std::io::Error;

    fn create_default() -> Result<Self, Self::Error> {
        let tty = open()?;
        Self::new(|| tty.try_clone())
    }

    fn render<'a, R>(
        &'a mut self,
        renderer: impl FnOnce(&mut Self::Renderer<'a>) -> R,
    ) -> Result<R, Self::Error> {
        Ok(renderer(&mut MioTtyRenderer::new(self)?))
    }
}

impl<W: Write + FromRawFd> MessageWriter for Tty<W> {
    type Error = std::io::Error;

    fn write_ansi(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1B[", b"\x9B")?;

        write_parameters(io.deref_mut(), parameters)?;
        io.write_all(intermediates.as_bytes())?;
        io.write_all(&[final_char as u8])?;

        io.flush()
    }

    fn write_dec(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1B[?", b"\x9B?")?;

        write_parameters(io.deref_mut(), parameters)?;
        io.write_all(intermediates.as_bytes())?;
        io.write_all(&[final_char as u8])?;

        io.flush()
    }

    fn write_dcs(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1BP", b"\x90")?;

        write_parameters(io.deref_mut(), parameters)?;
        io.write_all(intermediates.as_bytes())?;
        io.write_all(&[final_char as u8])?;
        io.write_all(data_string)?;

        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5C", b"\x9C")?;
        io.flush()
    }

    fn write_esc(&mut self, intermediates: &str, final_char: char) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();

        io.write_all(b"\x1B")?;
        io.write_all(intermediates.as_bytes())?;
        io.write_all(&[final_char as u8])?;

        io.flush()
    }

    fn write_print(&mut self, value: &[u8]) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();

        io.write_all(value)?;

        io.flush()
    }

    fn write_exec(&mut self, function: u8) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();

        io.write_all(&[function])?;

        io.flush()
    }

    fn write_osc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5D", b"\x9D")?;

        io.write_all(data_string)?;

        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5C", b"\x9C")?;
        io.flush()
    }

    fn write_pm(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5E", b"\x9E")?;

        io.write_all(data_string)?;

        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5C", b"\x9C")?;
        io.flush()
    }

    fn write_sos(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x58", b"\x98")?;

        io.write_all(data_string)?;

        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5C", b"\x9C")?;
        io.flush()
    }

    fn write_apc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let mut io = self.writer.lock();
        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5F", b"\x9F")?;

        io.write_all(data_string)?;

        self.write_8bit_alternate(io.deref_mut(), b"\x1B\x5C", b"\x9C")?;
        io.flush()
    }
}

#[inline]
fn write_parameters(io: &mut impl Write, p: &[&str]) -> Result<(), std::io::Error> {
    debug_assert!(
        p.len() < MAX_PARAMETERS,
        "{} parameters exceeds the specification limit of {}",
        p.len(),
        MAX_PARAMETERS
    );

    let mut need_semi = false;
    for p in p.iter().take(MAX_PARAMETERS) {
        if need_semi {
            io.write_all(b";")?;
        }
        need_semi = true;
        io.write_all(p.as_bytes())?;
    }
    Ok(())
}

impl<W: Write + FromRawFd> Tty<W> {
    fn write_8bit_alternate(
        &self,
        io: &mut impl Write,
        seven: &[u8],
        eight: &[u8],
    ) -> Result<(), std::io::Error> {
        #[cfg(feature = "eight_bit_vt")]
        const fn supported() -> bool {
            true
        }
        #[cfg(not(feature = "eight_bit_vt"))]
        const fn supported() -> bool {
            false
        }

        if supported() && self.state.is_8bit() {
            io.write_all(eight)
        } else {
            io.write_all(seven)
        }
    }

    fn wait_for_response(&mut self) -> Result<(), std::io::Error> {
        OperatingStatusRequest::new().write_to(self)?;

        if self.receiver.recv_timeout(Duration::from_secs(1)).is_err() {
            eprintln!("error");
            // TODO: log
        }
        Ok(())
    }

    fn enable_alternate_screen(&mut self) {
        if self.state.supports(Capability::AlternateScreen)
            && self.state.supports(Capability::SaveCursor)
        {
            self.is_alternate = DecModeSet::single(DecMode::AlternateScreenWithSaveCursor)
                .write_to(self)
                .is_ok();
        } else if self.state.supports(Capability::AlternateScreen) {
            self.is_alternate = DecModeSet::single(DecMode::AlternateScreen)
                .write_to(self)
                .is_ok();
        }
    }

    fn enable_raw_mode(&mut self, fd: FileDescriptor<W>) {
        (self.fd, self.original) = if let Some(original) = fd.use_fd(|fd| Termios::from_fd(fd)).ok()
        {
            use termios::*;
            let mut termios = original.clone();
            termios.c_iflag &= !(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
            termios.c_oflag &= !OPOST;
            termios.c_lflag &= !(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
            termios.c_cflag &= !(CSIZE | PARENB | CS5 | CS6 | CS7);
            termios.c_cflag |= CS8;

            if fd
                .use_fd(move |fd| {
                    termios::tcsetattr(fd, TCSANOW, &termios)?;
                    termios::tcflush(fd, TCIOFLUSH)
                })
                .is_ok()
            {
                (Some(fd), Some(original))
            } else {
                (None, None)
            }
        } else {
            (None, None)
        };
    }

    fn set_defaults(&mut self) -> Result<(), std::io::Error> {
        PrimaryDeviceAttributesRequest::new().write_to(self)?;
        for capability in [
            DecMode::SynchronizedOutput,
            DecMode::AlternateScreen,
            DecMode::SaveCursor,
        ] {
            DecModeRequest::new(capability).write_to(self)?;
        }
        self.wait_for_response()?;

        let version = self.state.version();
        SelectConformanceLevel::new(version, true);
        if version >= Version::Vt220 {
            self.state.set_8bit();
        }

        // TODO: 24bit color
        // TODO: keymap

        self.enable_alternate_screen();
        Ok(())
    }
}

impl<I: Send + Write + Read + FromRawFd + IntoRawFd + AsRawFd + 'static> Tty<I> {
    pub fn new(io: impl Fn() -> Result<I, std::io::Error>) -> Result<Self, std::io::Error> {
        let (r, w, fd) = (io()?, io()?, io()?);
        let (send, receiver) = flume::bounded(8);
        let worker_state = TtyState::new(send);
        let state = worker_state.clone();
        std::thread::spawn(move || Self::worker(r, worker_state));

        let mut result = Tty {
            writer: Mutex::new(BufWriter::new(w)),
            is_alternate: false,
            fd: None,
            original: None,
            state,
            receiver,
        };
        result.enable_raw_mode(FileDescriptor::new(fd));
        result.set_defaults()?;

        Ok(result)
    }

    fn worker(mut io: I, state: TtyState) {
        let mut buf = Buffer::default();

        let mut scanner = Scanner::new(Parser::new(state.create_router()));
        loop {
            if let Err(e) = buf.read_from(&mut io) {
                debug_assert_ok!(state.signal.send(Err(e)));
                return;
            }

            while buf.len() != 0 {
                if let Err(err) = scanner.scan(&mut buf) {
                    if matches!(err, crate::vt::ScannerError::InsufficientData) {
                        buf.ensure_capacity(1);
                    }
                }
            }
        }
    }
}

pub struct MioTtyRenderer<'a> {
    target: &'a mut Tty,
    sync: bool,
}

impl<'a> MioTtyRenderer<'a> {
    fn new(target: &'a mut Tty) -> Result<MioTtyRenderer, std::io::Error> {
        let sync = if target.state.supports(Capability::SynchronizedOutput) {
            DecModeSet::single(DecMode::SynchronizedOutput)
                .write_to(target)
                .is_ok()
        } else {
            false
        };
        Ok(Self { target, sync })
    }
}

impl Drop for MioTtyRenderer<'_> {
    fn drop(&mut self) {
        if self.sync {
            let result = DecModeReset::single(DecMode::SynchronizedOutput).write_to(self.target);
            crate::debug_assert_ok!(result);
        }
    }
}

impl<'a> TerminalRenderer for MioTtyRenderer<'a> {
    type Error = std::io::Error;

    fn print(&mut self, text: &str) -> Result<(), Self::Error> {
        Print::new(text).write_to(self.target)
    }

    fn set_style(&mut self, _fg: u16) -> Result<(), Self::Error> {
        Ok(())
    }

    fn reset_style(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

// #[cfg(test)]
// mod tests {
//     use crate::terminal::vt::dec;
//
//     use super::*;
//
//     #[test]
//     fn tty_state_8_bit() {
//         let (send, _) = flume::bounded(1);
//         let sut = TtyState::new(send);
//         sut.set_8bit();
//         assert!(sut.is_8bit());
//     }
//
//     #[test]
//     fn tty_state_capabilities() {
//         let (send, _) = flume::bounded(1);
//         let sut = TtyState::new(send);
//         sut.set_cap(Capability::Sixel);
//         sut.set_cap(Capability::Sixel);
//         sut.set_cap(Capability::PCTerm);
//         assert!(sut.supports(Capability::Sixel));
//         assert!(sut.supports(Capability::PCTerm));
//         assert!(!sut.supports(Capability::Level1));
//     }
//
//     #[test]
//     fn tty_state_primary_caps() {
//         let (send, _) = flume::bounded(1);
//         let sut = TtyState::new(send);
//         sut.set_primary_caps(&[vt::VT_420.into(), 44.into(), 1234.into()]);
//         assert_eq!(sut.version(), vt::VT_420);
//         assert!(sut.supports(Capability::Level1));
//         assert!(sut.supports(Capability::Level2));
//         assert!(sut.supports(Capability::Level3));
//         assert!(sut.supports(Capability::Level4));
//         assert!(sut.supports(Capability::PCTerm));
//         assert!(sut.supports(Capability::Unknown(1234)));
//         assert!(!sut.supports(Capability::Sixel));
//     }
//
//     #[test]
//     fn tty_state_synchronized_output() {
//         let (send, _) = flume::bounded(1);
//         let sut = TtyState::new(send);
//         sut.handle(&dec(
//             &[vt::MODE_SYNCHRONIZED_OUTPUT.into(), 1.into()],
//             b"$",
//             b'y',
//         ))
//         .unwrap();
//         assert!(sut.supports(Capability::SynchronizedOutput));
//     }
//
//     #[test]
//     fn tty_state_handle_primary_caps() {
//         let (send, receive) = flume::bounded(1);
//         let sut = TtyState::new(send);
//         sut.handle(&dec(&[vt::VT_420.into()], b"", b'c')).unwrap();
//         assert!(receive.try_recv().is_ok());
//         assert_eq!(sut.version(), vt::VT_420);
//     }
// }
