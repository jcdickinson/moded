mod buffer;

use std::{
    fs::File,
    marker::PhantomData,
    os::fd::{FromRawFd, IntoRawFd},
};

#[macro_export()]
macro_rules! debug_assert_ok {
    ($expr:expr) => {
        let _v = $expr;
        debug_assert!(_v.is_ok(), "{:?}", _v.unwrap_err());
    };
}

#[macro_export()]
macro_rules! assert_set_eq {
    ($a:expr, $b:expr) => {
        let mut a_vec: Vec<_> = $a.map(|x| x.to_owned()).collect();
        let mut b_vec: Vec<_> = $b.map(|x| x.to_owned()).collect();
        a_vec.sort();
        b_vec.sort();
        assert_eq!(a_vec, b_vec);
    };
}

#[macro_export]
#[cfg(test)]
macro_rules! tst {
    () => {
        println!("[{}:{}]", file!(), line!())
    };
    ($val:expr $(,)?) => {
        match $val {
            tmp => {
                println!("[{}:{}] {} = {:#?}",
                    file!(), line!(), stringify!($val), &tmp);
                tmp
            }
        }
    };
    ($($val:expr),+ $(,)?) => {
        ($($crate::tst!($val)),+,)
    };
}

#[macro_export]
#[cfg(not(test))]
macro_rules! tst {
    () => {
    };
    ($val:expr $(,)?) => {
        $val
    };
    ($($val:expr),+ $(,)?) => {
        ($($crate::tst!($val)),+,)
    };
}

pub use buffer::*;
use once_cell::sync::Lazy;

use crate::vt::MAX_PARAMETER_VALUE;

pub static NUMBERS: Lazy<Vec<&'static str>> = Lazy::new(|| {
    let max = std::u16::MAX as usize;
    let mut result = Vec::with_capacity(max as usize);

    for i in 0..max {
        let b = Box::leak(Box::new(i.to_string()));
        result.push(b.as_str());
    }

    result
});

pub struct FileDescriptor<T: FromRawFd = File> {
    fd: i32,
    p: PhantomData<T>,
}

impl<T: FromRawFd> FileDescriptor<T> {
    pub fn use_fd<R>(&self, f: impl FnOnce(i32) -> R) -> R {
        f(self.fd)
    }
}

impl<T: IntoRawFd + FromRawFd> FileDescriptor<T> {
    pub fn new(v: impl IntoRawFd) -> Self {
        Self {
            fd: v.into_raw_fd(),
            p: PhantomData,
        }
    }
}

impl<T: FromRawFd> Drop for FileDescriptor<T> {
    fn drop(&mut self) {
        unsafe { drop(T::from_raw_fd(self.fd)) };
    }
}

pub struct PrintableSeq<'a, T: AsRef<[u8]>>(pub &'a T);

impl<'a, T: AsRef<[u8]>> std::fmt::Debug for PrintableSeq<'a, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let r = self.0.as_ref();
        write!(f, "b\"")?;
        for c in r {
            match c {
                0x7F..=0xFF | 0x00..=0x1F => write!(f, "\\{:#04x}", c)?,
                b'\"' => write!(f, "\\\"")?,
                _ => write!(f, "{}", *c as char)?,
            }
        }
        write!(f, "\"")
    }
}

pub struct PrintableU8<'a, T: Into<u8> + Clone>(pub &'a T);

impl<'a, T: Into<u8> + Clone> std::fmt::Debug for PrintableU8<'a, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let r = Into::<u8>::into(self.0.clone());
        match r {
            0x7F..=0xFF | 0x00..=0x1F => write!(f, "{:#04x}", r),
            b'\'' => write!(f, "b'\\''"),
            _ => write!(f, "b'{}'", r as char),
        }
    }
}

#[cfg(any(test, debug, feature = "safe_vt_parsing"))]
pub fn bytes_to_utf8<'a>(v: &'a [u8]) -> &'a str {
    std::str::from_utf8(v).expect("the scanner to already have validated UTF-8")
}

#[cfg(not(any(test, debug, feature = "safe_vt_parsing")))]
pub fn bytes_to_utf8<'a>(v: &'a [u8]) -> &'a str {
    unsafe { std::str::from_utf8_unchecked(v) }
}

pub(crate) trait IntoNumber {
    fn into_number(&self) -> &'static str;
}

impl<T: Into<u16> + Clone> IntoNumber for T {
    fn into_number(&self) -> &'static str {
        let v: u16 = (self.clone()).into();
        debug_assert!(
            v <= MAX_PARAMETER_VALUE,
            "The parameter value {} is out of spec ({}).",
            v,
            MAX_PARAMETER_VALUE
        );

        let v = v.min(MAX_PARAMETER_VALUE);
        NUMBERS[v as usize]
    }
}
