use super::*;

impl<'a> ScannerSink for &[Token<'a>] {
    type Error = ();

    #[cfg_attr(coverage_nightly, no_coverage)]
    fn handle(&mut self, message: Token) -> Result<(), ()> {
        assert_ne!(self.len(), 0, "unexpected {:?}", &message);
        assert_eq!(&self[0], &message, "next in {:?}", self);
        *self = &self[1..];
        Ok(())
    }
}

impl<'a> ScannerSink for Vec<Token<'a>> {
    type Error = ();

    #[cfg_attr(coverage_nightly, no_coverage)]
    fn handle(&mut self, message: Token) -> Result<(), ()> {
        assert_ne!(self.len(), 0, "unexpected {:?}", &message);
        assert_eq!(&self[0], &message, "next in {:?}", self);
        self.remove(0);
        Ok(())
    }
}

macro_rules! simple_test {
        ($name: ident, $buffer:expr, [$($message:expr),+]) => {
            #[coverage_helper::test]
            fn $name() {
                use super::{Scanner, Token::*};
                let expected = [
                            $($message),+
                        ].as_ref();
                let mut parser = Scanner::new(expected);
                parser.scan(&mut Buffer::from_slice($buffer)).unwrap();
                assert_eq!(parser.into_inner(), &[]);
            }
        }
    }

simple_test!(
    print,
    b"hello world\x1Cgoodbye world",
    [
        Print(b"hello world"),
        Execute(0x1C),
        Print(b"goodbye world")
    ]
);

simple_test!(
    esc_dispatch,
    b"\x1B\\\x1B1\x1B\x7F\x7FU\x1B;\x1BZ\x1Bg",
    [
        Escape,
        EscapeDispatch(b'\\'),
        Escape,
        EscapeDispatch(b'1'),
        Escape,
        EscapeDispatch(b'U'),
        Escape,
        EscapeDispatch(b';'),
        Escape,
        EscapeDispatch(b'Z'),
        Escape,
        EscapeDispatch(b'g')
    ]
);

simple_test!(
    esc_intermediates,
    b"\x1B\x19 !\x7F\x7F#$\x00+,-5\x1B5",
    [
        Execute(0x19),
        Escape,
        Intermediate(b" !"),
        Intermediate(b"#$"),
        Execute(0x00),
        Intermediate(b"+,-"),
        EscapeDispatch(b'5'),
        Escape,
        EscapeDispatch(b'5')
    ]
);

simple_test!(
    csi_7bit_immediate_dispatch,
    b"\x1B[\x7F\x17\x7FZ\x1B[\x1F@",
    [
        ControlSequence,
        Execute(0x17),
        ControlSequenceDispatch(b'Z'),
        ControlSequence,
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

simple_test!(
    csi_8bit_immediate_dispatch,
    b"\x9B\x17Z\x9B\x1F@",
    [
        ControlSequence,
        Execute(0x17),
        ControlSequenceDispatch(b'Z'),
        ControlSequence,
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

simple_test!(
    csi_8bit_param_dispatch,
    b"\x9B5\x17100\x7F\x7F;>:\x00Z\x9B\x1F@",
    [
        ControlSequence,
        Param(b"5"),
        Execute(0x17),
        Param(b"100"),
        Param(b";>:"),
        Execute(0x00),
        ControlSequenceDispatch(b'Z'),
        ControlSequence,
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

simple_test!(
    csi_8bit_intermediate_dispatch,
    b"\x9B5\x17100;>:& \x00+\x7F\x7F&&Z\x9B& \x1F@",
    [
        ControlSequence,
        Param(b"5"),
        Execute(0x17),
        Param(b"100;>:"),
        Intermediate(b"& "),
        Execute(0x00),
        Intermediate(b"+"),
        Intermediate(b"&&"),
        ControlSequenceDispatch(b'Z'),
        ControlSequence,
        Intermediate(b"& "),
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

simple_test!(
    csi_8bit_intermediate_ignore,
    b"\x9B5\x17100;>:&0 \x00+\x7F\x7F&&Z\x9B& \x1F@",
    [
        ControlSequence,
        Param(b"5"),
        Execute(0x17),
        Param(b"100;>:"),
        Intermediate(b"&"),
        Clear,
        Execute(0x00),
        ControlSequence,
        Intermediate(b"& "),
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

simple_test!(
    dcs_7bit_immediate_dispatch,
    b"\x1BP\x7F\x17\x7FZ Test\x17&#\x7F*\x1B\\\x1BPY\x1B\\",
    [
        DeviceControlString,
        DeviceControlStringDispatch(b'Z'),
        Put(b" Test\x17&#"),
        Put(b"*"),
        End,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    dcs_8bit_immediate_dispatch,
    b"\x90\x7F\x17\x7FZ Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        DeviceControlString,
        DeviceControlStringDispatch(b'Z'),
        Put(b" Test\x17&#"),
        Put(b"*"),
        End,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    dcs_8bit_params,
    b"\x90\x7F100;<?\x17\x7F:=Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        DeviceControlString,
        Param(b"100;<?"),
        Param(b":="),
        DeviceControlStringDispatch(b'Z'),
        Put(b" Test\x17&#"),
        Put(b"*"),
        End,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    dcs_8bit_params_intermediates,
    b"\x90\x7F100;<?\x17\x7F:=+,/\x19()Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        DeviceControlString,
        Param(b"100;<?"),
        Param(b":="),
        Intermediate(b"+,/"),
        Intermediate(b"()"),
        DeviceControlStringDispatch(b'Z'),
        Put(b" Test\x17&#"),
        Put(b"*"),
        End,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    dcs_8bit_intermediates,
    b"\x90\x7F+,/\x19()Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        DeviceControlString,
        Intermediate(b"+,/"),
        Intermediate(b"()"),
        DeviceControlStringDispatch(b'Z'),
        Put(b" Test\x17&#"),
        Put(b"*"),
        End,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    dcs_7bit_ignore,
    b"\x1BP\x7F100;<?\x17\x7F:=+,/\x19()100Z Test\x17&#\x7F*\x1B\\\x1BPY\x1B\\",
    [
        DeviceControlString,
        Param(b"100;<?"),
        Param(b":="),
        Intermediate(b"+,/"),
        Intermediate(b"()"),
        Clear,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    dcs_8bit_ignore,
    b"\x90\x7F100;<?\x17\x7F:=+,/\x19()100Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        DeviceControlString,
        Param(b"100;<?"),
        Param(b":="),
        Intermediate(b"+,/"),
        Intermediate(b"()"),
        Clear,
        DeviceControlString,
        DeviceControlStringDispatch(b'Y'),
        End
    ]
);

simple_test!(
    csi_8bit_can,
    b"\x9B5\x17100\x18\x9B\x1F@",
    [
        ControlSequence,
        Param(b"5"),
        Execute(0x17),
        Param(b"100"),
        Clear,
        ControlSequence,
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

simple_test!(
    csi_8bit_sub,
    b"\x9B5\x17100\x1A\x9B\x1F@",
    [
        ControlSequence,
        Param(b"5"),
        Execute(0x17),
        Param(b"100"),
        Clear,
        Substitute,
        ControlSequence,
        Execute(0x1F),
        ControlSequenceDispatch(b'@')
    ]
);

macro_rules! terminated_string_test(
        ($name:ident, $char: literal, $type:ident) => {
        mod $name {
            use super::*;
            simple_test!(
                with_7bit,
                {
                    let mut v = b"\x1B".to_vec();
                    v.push($char);
                    v.extend_from_slice(b"\x17\x19\x1E \x1D&%\x7FYolo\x1B\\\x1B");
                    v.push($char);
                    v.extend_from_slice(b"Test\x1B\\");
                    v
                },
                [
                    $type,
                    Put(b"\x17\x19\x1E \x1D&%"),
                    Put(b"Yolo"),
                    End,
                    $type,
                    Put(b"Test"),
                    End
                ]
            );
            simple_test!(
                with_8bit,
                {
                    let mut v = Vec::new();
                    v.push($char + 0x40);
                    v.extend_from_slice(b"\x17\x19\x1E \x1D&%\x7FYolo\x9C");
                    v.push($char + 0x40);
                    v.extend_from_slice(b"Test\x9C");
                    v
                },
                [
                    $type,
                    Put(b"\x17\x19\x1E \x1D&%"),
                    Put(b"Yolo"),
                    End,
                    $type,
                    Put(b"Test"),
                    End
                ]
            );
            simple_test!(
                cancelled,
                {
                    let mut v = Vec::new();
                    v.push($char + 0x40);
                    v.extend_from_slice(b"\x17\x19\x1E \x1D&%\x7FYolo\x8D\x9C");
                    v
                },
                [
                    $type,
                    Put(b"\x17\x19\x1E \x1D&%"),
                    Put(b"Yolo"),
                    Clear,
                    Execute(0x8D),
                    Clear
                ]
            );
        }
    });

terminated_string_test!(osc, b']', OperatingSystemCommand);
terminated_string_test!(sos, b'X', String);
terminated_string_test!(pm, b'^', PrivacyMessage);
terminated_string_test!(apc, b'_', ApplicationProgramCommand);
