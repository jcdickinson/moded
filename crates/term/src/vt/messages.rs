mod ansi;
mod dcs;
mod dec;
mod router;
mod test_writer;

use num_enum::{FromPrimitive, IntoPrimitive, TryFromPrimitive};

pub use ansi::*;
pub use dcs::*;
pub use dec::*;
pub use router::*;

pub const MAX_PARAMETERS: usize = 16;
pub const MAX_PARAMETER_VALUE: u16 = 9999;

pub trait MessageWriter {
    type Error;
    fn write_ansi(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error>;
    fn write_dec(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error>;
    fn write_dcs(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<(), Self::Error>;
    fn write_esc(&mut self, intermediates: &str, final_char: char) -> Result<(), Self::Error>;
    fn write_osc(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn write_pm(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn write_sos(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn write_apc(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn write_print(&mut self, value: &[u8]) -> Result<(), Self::Error>;
    fn write_exec(&mut self, function: u8) -> Result<(), Self::Error>;
}

#[allow(unused_variables)]
pub trait Message: Sized {
    fn create_filters() -> Vec<MessageFilter>;
    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error>;

    fn try_parse_ansi(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        None
    }
    fn try_parse_dec(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        None
    }
    fn try_parse_dcs(
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Option<Self> {
        None
    }
    fn try_parse_exec(function: u8) -> Option<Self> {
        None
    }
    fn try_parse_print(value: &[u8]) -> Option<Self> {
        None
    }
    fn try_parse_esc(intermediates: &str, final_char: char) -> Option<Self> {
        None
    }
    fn try_parse_osc(data_string: &[u8]) -> Option<Self> {
        None
    }
    fn try_parse_pm(data_string: &[u8]) -> Option<Self> {
        None
    }
    fn try_parse_sos(data_string: &[u8]) -> Option<Self> {
        None
    }
    fn try_parse_apc(data_string: &[u8]) -> Option<Self> {
        None
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, IntoPrimitive, TryFromPrimitive)]
#[repr(u16)]
pub enum ModeSetting {
    NotRecognized = 0,
    Set = 1,
    Reset = 2,
    PermanentlySet = 3,
    PermanentReset = 4,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, IntoPrimitive, FromPrimitive)]
#[repr(u16)]
pub enum PrimaryDeviceAttribute {
    Sixel = 4,
    PcTerm = 44,
    #[num_enum(catch_all)]
    Unknown(u16),
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Print(Box<[u8]>);

impl Print {
    pub fn new(bytes: impl AsRef<[u8]>) -> Self {
        Self(bytes.as_ref().into())
    }
}

impl<T: AsRef<[u8]>> From<T> for Print {
    fn from(value: T) -> Self {
        Self(value.as_ref().into())
    }
}

impl Message for Print {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::print()]
    }

    fn try_parse_print(value: &[u8]) -> Option<Self> {
        Some(Self(value.into()))
    }

    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        writer.write_print(self.0.as_ref())
    }
}
