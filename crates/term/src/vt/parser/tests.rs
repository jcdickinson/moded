use crate::{
    util::{Buffer, PrintableSeq, PrintableU8},
    vt::Scanner,
};

use super::*;

#[derive(PartialEq, Eq)]
enum Message {
    Dcs(Vec<String>, String, char, Vec<u8>),
    Ansi(Vec<String>, String, char),
    Dec(Vec<String>, String, char),
    Esc(String, char),
    Osc(Vec<u8>),
    Sos(Vec<u8>),
    Apc(Vec<u8>),
    Pm(Vec<u8>),
    Print(Vec<u8>),
    Exec(u8),
    Sub(),
}

impl std::fmt::Debug for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Dcs(arg0, arg1, arg2, arg3) => f
                .debug_tuple("Dcs")
                .field(&arg0.iter().map(|x| PrintableSeq(x)).collect::<Vec<_>>())
                .field(arg1)
                .field(arg2)
                .field(&PrintableSeq(arg3))
                .finish(),
            Self::Ansi(arg0, arg1, arg2) => f
                .debug_tuple("Ansi")
                .field(&arg0.iter().map(|x| PrintableSeq(x)).collect::<Vec<_>>())
                .field(arg1)
                .field(arg2)
                .finish(),
            Self::Dec(arg0, arg1, arg2) => f
                .debug_tuple("Dec")
                .field(&arg0.iter().map(|x| PrintableSeq(x)).collect::<Vec<_>>())
                .field(arg1)
                .field(arg2)
                .finish(),
            Self::Esc(arg0, arg1) => f.debug_tuple("UnknownEsc").field(arg0).field(arg1).finish(),
            Self::Osc(arg0) => f.debug_tuple("Osc").field(&PrintableSeq(arg0)).finish(),
            Self::Apc(arg0) => f.debug_tuple("Apc").field(&PrintableSeq(arg0)).finish(),
            Self::Sos(arg0) => f.debug_tuple("Sos").field(&PrintableSeq(arg0)).finish(),
            Self::Pm(arg0) => f.debug_tuple("Pm").field(&PrintableSeq(arg0)).finish(),
            Self::Print(arg0) => f.debug_tuple("Print").field(&PrintableSeq(arg0)).finish(),
            Self::Exec(c) => f.debug_tuple("Exec").field(&PrintableU8(c)).finish(),
            Self::Sub() => f.debug_tuple("Sub").finish(),
        }
    }
}

impl Message {
    pub fn dcs(p: &[&str], i: &str, f: char, s: &[u8]) -> Self {
        Self::Dcs(
            p.into_iter().map(|x| x.to_string()).collect(),
            i.to_string(),
            f,
            s.to_vec(),
        )
    }

    pub fn dec(p: &[&str], i: &str, f: char) -> Self {
        Self::Dec(
            p.into_iter().map(|x| x.to_string()).collect(),
            i.to_string(),
            f,
        )
    }

    pub fn ansi(p: &[&str], i: &str, f: char) -> Self {
        Self::Ansi(
            p.into_iter().map(|x| x.to_string()).collect(),
            i.to_string(),
            f,
        )
    }

    pub fn print(i: &[u8]) -> Self {
        Self::Print(i.to_vec())
    }

    pub fn exec(c: u8) -> Self {
        Self::Exec(c)
    }

    pub fn esc(i: &str, f: char) -> Self {
        Self::Esc(i.to_string(), f)
    }

    pub fn sub() -> Self {
        Self::Sub()
    }

    pub fn osc(i: &[u8]) -> Self {
        Self::Osc(i.to_vec())
    }

    pub fn pm(i: &[u8]) -> Self {
        Self::Pm(i.to_vec())
    }

    pub fn apc(i: &[u8]) -> Self {
        Self::Apc(i.to_vec())
    }

    pub fn sos(i: &[u8]) -> Self {
        Self::Sos(i.to_vec())
    }
}

#[derive(Debug)]
struct TestParserSink {
    results: Vec<Message>,
}

impl TestParserSink {
    fn new() -> Self {
        Self {
            results: Vec::new(),
        }
    }
}

impl ParserSink for TestParserSink {
    type Error = ();

    fn ansi(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        let parameters = parameters.iter().map(|x| x.to_string()).collect();
        let intermediates = intermediates.into();
        let final_char = final_char.into();
        self.results
            .push(Message::Ansi(parameters, intermediates, final_char));
        Ok(())
    }

    fn dec(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        let parameters = parameters.iter().map(|x| x.to_string()).collect();
        let intermediates = intermediates.into();
        let final_char = final_char.into();
        self.results
            .push(Message::Dec(parameters, intermediates, final_char));
        Ok(())
    }

    fn print(&mut self, value: &[u8]) -> Result<(), Self::Error> {
        let value = value.into();
        self.results.push(Message::Print(value));
        Ok(())
    }

    fn exec(&mut self, function: u8) -> Result<(), Self::Error> {
        self.results.push(Message::Exec(function));
        Ok(())
    }

    fn esc(&mut self, intermediates: &str, final_char: char) -> Result<(), Self::Error> {
        let intermediates = intermediates.into();
        self.results.push(Message::Esc(intermediates, final_char));
        Ok(())
    }

    fn dcs(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<(), Self::Error> {
        let parameters = parameters.iter().map(|x| x.to_string()).collect();
        let intermediates = intermediates.into();
        let data_string = data_string.into();
        self.results.push(Message::Dcs(
            parameters,
            intermediates,
            final_char,
            data_string,
        ));
        Ok(())
    }

    fn substitute(&mut self) -> Result<(), Self::Error> {
        self.results.push(Message::Sub());
        Ok(())
    }

    fn osc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let data_string = data_string.into();
        self.results.push(Message::Osc(data_string));
        Ok(())
    }

    fn pm(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let data_string = data_string.into();
        self.results.push(Message::Pm(data_string));
        Ok(())
    }

    fn apc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let data_string = data_string.into();
        self.results.push(Message::Apc(data_string));
        Ok(())
    }

    fn sos(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        let data_string = data_string.into();
        self.results.push(Message::Sos(data_string));
        Ok(())
    }
}

macro_rules! simple_test {
        ($name: ident, $buffer:expr, [$($message:expr),*]) => {
            #[coverage_helper::test]
            fn $name() {
                let sink = TestParserSink::new();
                let parser = Parser::new(sink);
                let mut scanner = Scanner::new(parser);
                scanner.scan(&mut Buffer::from_slice($buffer)).unwrap();
                let results = scanner.into_inner().into_inner().results;
                assert_eq!(results, [
                    $($message),*
                ].as_ref());
            }
        } }

simple_test!(
    print,
    b"hello\x1Cworld",
    [
        Message::print(b"hello"),
        Message::exec(0x1C),
        Message::print(b"world")
    ]
);

simple_test!(
    esc_dispatch,
    b"\x1B\\\x1B1\x1B\x7F\x7FU\x1B;\x1BZ\x1Bg",
    [
        Message::esc("", '\\'),
        Message::esc("", '1'),
        Message::esc("", 'U'),
        Message::esc("", ';'),
        Message::esc("", 'Z'),
        Message::esc("", 'g')
    ]
);

simple_test!(
    esc_intermediates,
    b"\x1B\x19 !\x7F\x7F#$\x00+,-5\x1B5",
    [
        Message::exec(0x19),
        Message::exec(0x00),
        Message::esc(" !#$+,-", '5'),
        Message::esc("", '5')
    ]
);

simple_test!(
    ansi_params_intermediates,
    b"\x9B5100;>:& +\x7F\x7F&&Z",
    [Message::ansi(&["5100", ">:"], "& +&&", 'Z')]
);

simple_test!(
    ansi_params,
    b"\x9B5100;>:Z",
    [Message::ansi(&["5100", ">:"], "", 'Z')]
);

simple_test!(
    ansi_params_empty_first,
    b"\x9B;>:Z",
    [Message::ansi(&["0", ">:"], "", 'Z')]
);

simple_test!(
    ansi_params_empty_last,
    b"\x9B10;Z",
    [Message::ansi(&["10", "0"], "", 'Z')]
);

simple_test!(
    ansi_params_empty_middle,
    b"\x9B10;;20Z",
    [Message::ansi(&["10", "0", "20"], "", 'Z')]
);

simple_test!(
    ansi_intermediates,
    b"\x9B& +\x7F\x7F&&Z",
    [Message::ansi(&["0"], "& +&&", 'Z')]
);

simple_test!(
    dec_params_intermediates,
    b"\x9B?5100;>:& +\x7F\x7F&&Z",
    [Message::dec(&["5100", ">:"], "& +&&", 'Z')]
);

simple_test!(
    dec_params,
    b"\x9B?5100;>:Z",
    [Message::dec(&["5100", ">:"], "", 'Z')]
);

simple_test!(
    dec_params_empty_first,
    b"\x9B?;>:Z",
    [Message::dec(&["0", ">:"], "", 'Z')]
);

simple_test!(
    dec_last,
    b"\x9B?10;Z",
    [Message::dec(&["10", "0"], "", 'Z')]
);

simple_test!(
    dec_params_empty_middle,
    b"\x9B?10;;20Z",
    [Message::dec(&["10", "0", "20"], "", 'Z')]
);

simple_test!(
    dec_intermediates,
    b"\x9B?& +\x7F\x7F&&Z",
    [Message::dec(&["0"], "& +&&", 'Z')]
);

simple_test!(
    dcs_immediate_dispatch,
    b"\x90\x7F\x17\x7FZ Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        Message::dcs(&["0"], "", 'Z', b" Test\x17&#*"),
        Message::dcs(&["0"], "", 'Y', &[])
    ]
);

simple_test!(
    dcs_params,
    b"\x90\x7F100;<?\x17\x7F:=Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        Message::dcs(&["100", "<?:="], "", 'Z', b" Test\x17&#*"),
        Message::dcs(&["0"], "", 'Y', &[])
    ]
);

simple_test!(
    dcs_params_intermediates,
    b"\x90\x7F100;<?\x17\x7F:=+,/\x19()Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        Message::dcs(&["100", "<?:="], "+,/()", 'Z', b" Test\x17&#*"),
        Message::dcs(&["0"], "", 'Y', &[])
    ]
);

simple_test!(
    dcs_intermediates,
    b"\x90\x7F+,/\x19()Z Test\x17&#\x7F*\x9C\x90Y\x9C",
    [
        Message::dcs(&["0"], "+,/()", 'Z', b" Test\x17&#*"),
        Message::dcs(&["0"], "", 'Y', &[])
    ]
);

simple_test!(
    can,
    b"\x9B5\x17100\x18\x9B\x1F@",
    [
        Message::exec(0x17),
        Message::exec(0x1F),
        Message::ansi(&["0"], "", '@')
    ]
);

simple_test!(
    sub,
    b"\x9B5\x17100\x1A\x9B\x1F@",
    [
        Message::exec(0x17),
        Message::sub(),
        Message::exec(0x1F),
        Message::ansi(&["0"], "", '@')
    ]
);

macro_rules! terminated_string_test(
        ($name:ident, $char: literal, $factory:ident) => {
        mod $name {
            use super::*;
            simple_test!(
                regular,
                {
                    let mut v = Vec::new();
                    v.push($char + 0x40);
                    v.extend_from_slice(b"\x17\x19\x1E \x1D&%\x7FYolo\x9C");
                    v.push($char + 0x40);
                    v.extend_from_slice(b"Test\x9C");
                    v
                },
                [
                    Message::$factory(b"\x17\x19\x1E \x1D&%Yolo"),
                    Message::$factory(b"Test")
                ]
            );
            simple_test!(
                cancelled,
                {
                    let mut v = Vec::new();
                    v.push($char + 0x40);
                    v.extend_from_slice(b"\x17\x19\x1E \x1D&%\x7FYolo\x8D\x9C");
                    v
                },
                [
                    Message::exec(0x8D)
                ]
            );
        }
    });

terminated_string_test!(osc, b']', osc);
terminated_string_test!(sos, b'X', sos);
terminated_string_test!(pm, b'^', pm);
terminated_string_test!(apc, b'_', apc);
