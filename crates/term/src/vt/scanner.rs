use crate::util::{Buffer, PrintableSeq, PrintableU8};

/// This is responsible for parsing the different [`Token`] types that are
/// emitted from the [`Scanner`].
pub trait ScannerSink {
    type Error;

    /// Handles the next [`Token`] emitted by the [`Scanner`].
    fn handle(&mut self, token: Token) -> Result<(), Self::Error>;
}

/// Directly implements the VT parsing state machine.
pub struct Scanner<T: ScannerSink> {
    state: State,
    handler: T,
    next_state: Option<State>,
}

/// Errors returned by the [`Scanner`].
pub enum ScannerError<T> {
    /// More data is required to continue scanning.
    InsufficientData,
    ParserError(T),
}

impl<T: std::fmt::Display> std::fmt::Display for ScannerError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InsufficientData => write!(f, "insufficient data to parse the message"),
            Self::ParserError(arg0) => write!(f, "the interior parser failed: {}", arg0),
        }
    }
}

impl<T: std::fmt::Debug> std::fmt::Debug for ScannerError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InsufficientData => write!(f, "insufficient data to parse the message"),
            Self::ParserError(arg0) => write!(f, "the interior parser failed: {:?}", arg0),
        }
    }
}

impl<T: std::fmt::Debug + std::error::Error> std::error::Error for ScannerError<T> {}

#[derive(Debug, PartialEq, Eq)]
enum State {
    Ground,
    Escape,
    StringTerminated,
    DcsEntry,
    CsEntry,
    EscapeIntermediate,
    CsIntermediate,
    CsIgnore,
    CsParam,
    DcsParam,
    DcsIntermediate,
    DcsIgnore,
}

/// The tokens that the [`Scanner`] emits to the [`ScannerSink`].
#[derive(Clone, PartialEq, Eq)]
pub enum Token<'a> {
    /// The 'clear' action .
    Clear,
    /// The 'SUB' VT signal.
    Substitute,
    /// The 'put' action.
    Put(&'a [u8]),
    /// The 'param' action.
    Param(&'a [u8]),
    /// The 'collect' action for intermediates.
    Intermediate(&'a [u8]),
    /// The 'execute' action.
    Execute(u8),
    /// The 'print' action.
    Print(&'a [u8]),
    /// The 'entry' action for 'escape'.
    Escape,
    /// The 'esc_dispatch' action .
    EscapeDispatch(u8),
    /// The 'entry' action for 'csi entry'.
    ControlSequence,
    /// The 'csi_dispatch' action.
    ControlSequenceDispatch(u8),
    /// The 'entry' action for 'dcs entry'.
    DeviceControlString,
    /// The 'csi_dispatch' action.
    DeviceControlStringDispatch(u8),
    /// When the 'sos' state is entered.
    String,
    /// When the 'pm' state is entered,
    PrivacyMessage,
    /// When the 'apc' state is entered.
    ApplicationProgramCommand,
    /// When the 'osc' state is entered,
    OperatingSystemCommand,
    /// The '{sos,pm,apc}_end' actions in the state_machine.
    End,
}

impl<'a> std::fmt::Debug for Token<'a> {
    #[cfg_attr(coverage_nightly, no_coverage)]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Clear => write!(f, "Clear"),
            Self::Substitute => write!(f, "Substitute"),
            Self::Put(arg0) => f.debug_tuple("Put").field(&PrintableSeq(arg0)).finish(),
            Self::Param(arg0) => f.debug_tuple("Param").field(&PrintableSeq(arg0)).finish(),
            Self::Intermediate(arg0) => f
                .debug_tuple("Intermediate")
                .field(&PrintableSeq(arg0))
                .finish(),
            Self::Execute(arg0) => f.debug_tuple("Execute").field(&PrintableU8(arg0)).finish(),
            Self::Print(arg0) => f.debug_tuple("Print").field(&PrintableSeq(arg0)).finish(),
            Self::Escape => write!(f, "Escape"),
            Self::EscapeDispatch(arg0) => f
                .debug_tuple("EscapeDispatch")
                .field(&PrintableU8(arg0))
                .finish(),
            Self::ControlSequence => write!(f, "ControlSequence"),
            Self::ControlSequenceDispatch(arg0) => f
                .debug_tuple("ControlSequenceDispatch")
                .field(&PrintableU8(arg0))
                .finish(),
            Self::DeviceControlString => write!(f, "DeviceControlString"),
            Self::DeviceControlStringDispatch(arg0) => f
                .debug_tuple("DeviceControlStringDispatch")
                .field(&PrintableU8(arg0))
                .finish(),
            Self::String => write!(f, "String"),
            Self::PrivacyMessage => write!(f, "PrivacyMessage"),
            Self::ApplicationProgramCommand => write!(f, "ApplicationProgramCommand"),
            Self::OperatingSystemCommand => write!(f, "OperatingSystemCommand"),
            Self::End => write!(f, "End"),
        }
    }
}

enum InternalError<T: ScannerSink> {
    DataRequired,
    Unknown,
    ParserError(T::Error),
    SkipToState,
}

impl<T: ScannerSink> Scanner<T> {
    /// Creates a new parser.
    pub fn new(handler: T) -> Self {
        Self {
            state: State::Ground,
            handler,
            next_state: None,
        }
    }

    /// Consumes the scanner and returns the contained [`ScannerSink`].
    pub fn into_inner(self) -> T {
        self.handler
    }

    fn raise(&mut self, message: Token) -> Result<(), InternalError<T>> {
        self.handler
            .handle(message)
            .map_err(|e| InternalError::ParserError(e))
    }

    /// Scans and consumes the contents of the given buffer, while providing
    /// data the the [`ScannerSink`] as complete tokens are lexed.
    pub fn scan(&mut self, buffer: &mut Buffer) -> Result<(), ScannerError<T::Error>> {
        use InternalError::*;
        let mut token = None;
        while buffer
            .must_advance(&mut token)
            .ok_or_else(|| ScannerError::InsufficientData)?
        {
            if let Some(next_state) = self.next_state.take() {
                self.transition(next_state)
                    .map_err(ScannerError::ParserError)?;
            }

            let result = self.delegate(buffer);
            match result {
                Err(SkipToState) => {
                    token = None;
                    Ok(())
                }
                Err(DataRequired) => Err(ScannerError::InsufficientData),
                Err(Unknown) => {
                    buffer.consume(1);
                    Ok(())
                }
                Err(ParserError(e)) => Err(ScannerError::ParserError(e)),
                Ok(_) => Ok(()),
            }?;
        }
        Ok(())
    }

    fn delegate(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        match self.state {
            State::Ground => self.ground(buffer),
            State::Escape => self.escape(buffer),
            State::StringTerminated => self.string_terminated(buffer),
            State::DcsEntry => self.device_control_string_entry(buffer),
            State::CsEntry => self.control_sequence_entry(buffer),
            State::EscapeIntermediate => self.escape_intermediate(buffer),
            State::CsIntermediate => self.control_sequence_intermediate(buffer),
            State::CsIgnore => self.control_sequence_ignore(buffer),
            State::CsParam => self.control_sequence_param(buffer),
            State::DcsParam => self.device_control_string_param(buffer),
            State::DcsIntermediate => self.device_control_string_intermediate(buffer),
            State::DcsIgnore => self.device_control_string_ignore(buffer),
        }
    }

    fn transition(&mut self, new: State) -> Result<(), T::Error> {
        use State::*;
        if self.state == new {
            return Ok(());
        }

        tst!(&self.state, &new);
        let result = match new {
            CsEntry => self.handler.handle(Token::ControlSequence),
            CsIgnore => self.handler.handle(Token::Clear),
            DcsEntry => self.handler.handle(Token::DeviceControlString),
            DcsIgnore => self.handler.handle(Token::Clear),
            _ => Ok(()),
        };
        self.state = new;

        result
    }

    fn transition_to_string_terminated(
        &mut self,
        buffer: &mut Buffer,
        is_dcs: bool,
    ) -> Result<(), InternalError<T>> {
        self.next_state = Some(State::StringTerminated);

        let state = buffer[0];
        buffer.consume(1);
        match (is_dcs, state) {
            (true, s) => self.raise(Token::DeviceControlStringDispatch(s))?,
            (false, 0x58 | 0x98) => self.raise(Token::String)?,
            (false, 0x5D | 0x9D) => self.raise(Token::OperatingSystemCommand)?,
            (false, 0x5E | 0x9E) => self.raise(Token::PrivacyMessage)?,
            (false, 0x5F | 0x9F) => self.raise(Token::ApplicationProgramCommand)?,
            _ => unreachable!(),
        }
        Err(InternalError::SkipToState)
    }

    fn consume_escape(&self, buffer: &mut Buffer) {
        match buffer[..] {
            [0x1B, _, ..] => buffer.consume(2),
            [_, ..] => buffer.consume(1),
            _ => 0,
        };
    }

    #[inline]
    fn anywhere_or_escape(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        match buffer[..] {
            [0x1B, ..] => {
                buffer.consume(1);
                self.next_state = Some(State::Escape);
                Err(InternalError::SkipToState)
            }
            [0x9C, ..] => {
                buffer.consume(1);
                self.next_state = Some(State::Ground);
                Err(InternalError::SkipToState)
            }
            _ => self.anywhere(buffer),
        }
    }

    #[inline]
    fn anywhere(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        match buffer[..] {
            [0x18, ..] => {
                buffer.consume(1);
                self.next_state = Some(State::Ground);
                self.raise(Token::Clear)?;
                Err(InternalError::SkipToState)
            }
            [0x1A, ..] => {
                self.next_state = Some(State::Ground);
                buffer.consume(1);
                self.raise(Token::Clear)?;
                self.raise(Token::Substitute)?;
                Err(InternalError::SkipToState)
            }
            [0x80..=0x8F | 0x91..=0x97 | 0x99 | 0x9A, ..] => {
                self.next_state = Some(State::Ground);
                buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
                self.raise(Token::Clear)?;
                Err(InternalError::SkipToState)
            }
            [0x98 | 0x9D..=0x9F, ..] => self.transition_to_string_terminated(buffer, false),
            [0x90, ..] => {
                self.next_state = Some(State::DcsEntry);
                buffer.consume(1);
                Err(InternalError::SkipToState)
            }
            [0x9B, ..] => {
                self.next_state = Some(State::CsEntry);
                buffer.consume(1);
                Err(InternalError::SkipToState)
            }
            _ => Ok(()),
        }
    }

    fn ground(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            while matches!(buffer[..], [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..]) {
                buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x20..=0x7F, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Print(b)))?;
            }
        }

        Ok(())
    }

    fn escape(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            while matches!(buffer[..], [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..]) {
                buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x58 | 0x5D..=0x5F, ..] => self.transition_to_string_terminated(buffer, false),
                [0x50, ..] => {
                    self.next_state = Some(State::DcsEntry);
                    buffer.consume(1);
                    Err(InternalError::SkipToState)
                }
                [0x5B, ..] => {
                    self.next_state = Some(State::CsEntry);
                    buffer.consume(1);
                    Err(InternalError::SkipToState)
                }
                [0x20..=0x2F, ..] => {
                    self.next_state = Some(State::EscapeIntermediate);
                    self.raise(Token::Escape)?;
                    Err(InternalError::SkipToState)
                }
                [0x30..=0x4F | 0x51..=0x57 | 0x59 | 0x5A | 0x5C | 0x60..=0x7E, ..] => {
                    self.next_state = Some(State::Ground);
                    buffer.consume_with(1, |b| {
                        let ra = self.raise(Token::Escape);
                        let rb = self.raise(Token::EscapeDispatch(b[0]));
                        ra.and(rb)
                    })?;
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn escape_intermediate(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x20..=0x2F, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Intermediate(b)))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                    buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
                    Ok(())
                }
                [0x30..=0x7E, ..] => {
                    self.next_state = Some(State::Ground);
                    buffer.consume_with(1, |b| self.raise(Token::EscapeDispatch(b[0])))?;
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn string_terminated(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            let mut i = 0;
            while matches!(buffer[i..], [0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            let mut i = 0;
            while matches!(buffer[i..], [0x00..=0x17 | 0x19 | 0x1C..=0x7E, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Put(b)))?;
            }

            match buffer[..] {
                [0x1B, 0x5C, ..] | [0x9C, ..] => {
                    self.next_state = Some(State::Ground);
                    self.consume_escape(buffer);
                    self.raise(Token::End)?;
                    Err(InternalError::SkipToState)
                }
                [0x1B] => Err(InternalError::DataRequired),
                [0x1B, _, ..]
                | [0x1A | 0x84 | 0x85 | 0x88 | 0x8D..=0x8F | 0x90 | 0x98 | 0x9A..=0x9F, ..] => {
                    self.next_state = Some(State::Ground);
                    self.raise(Token::Clear)?;
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn control_sequence_entry(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                    buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
                    Ok(())
                }
                [0x40..=0x7E, ..] => {
                    self.next_state = Some(State::Ground);
                    buffer.consume_with(1, |b| self.raise(Token::ControlSequenceDispatch(b[0])))?;
                    Err(InternalError::SkipToState)
                }
                [0x20..=0x2F, ..] => {
                    self.next_state = Some(State::CsIntermediate);
                    Err(InternalError::SkipToState)
                }
                [0x30..=0x3F, ..] => {
                    self.next_state = Some(State::CsParam);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn control_sequence_param(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x30..=0x3F, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Param(b)))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                    buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
                    Ok(())
                }
                [0x40..=0x7E, ..] => {
                    self.next_state = Some(State::Ground);
                    buffer.consume_with(1, |b| self.raise(Token::ControlSequenceDispatch(b[0])))?;
                    Err(InternalError::SkipToState)
                }
                [0x20..=0x2F, ..] => {
                    self.next_state = Some(State::CsIntermediate);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn control_sequence_intermediate(
        &mut self,
        buffer: &mut Buffer,
    ) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x20..=0x2F, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Intermediate(b)))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                    buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
                    Ok(())
                }
                [0x40..=0x7E, ..] => {
                    self.next_state = Some(State::Ground);
                    buffer.consume_with(1, |b| self.raise(Token::ControlSequenceDispatch(b[0])))?;
                    Err(InternalError::SkipToState)
                }
                [0x30..=0x3F, ..] => {
                    self.next_state = Some(State::CsIgnore);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn control_sequence_ignore(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x20..=0x3F | 0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..] => {
                    buffer.consume_with(1, |b| self.raise(Token::Execute(b[0])))?;
                    Ok(())
                }
                [0x40..=0x7E, ..] => {
                    self.next_state = Some(State::Ground);
                    buffer.consume(1);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn device_control_string_entry(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x00..=0x17 | 0x19 | 0x1C..=0x1F | 0x7F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x40..=0x7E, ..] => self.transition_to_string_terminated(buffer, true),
                [0x30..=0x3F, ..] => {
                    self.next_state = Some(State::DcsParam);
                    Err(InternalError::SkipToState)
                }
                [0x20..=0x2F, ..] => {
                    self.next_state = Some(State::DcsIntermediate);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn device_control_string_param(&mut self, buffer: &mut Buffer) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x30..=0x3F, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Param(b)))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x7F | 0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x40..=0x7E, ..] => self.transition_to_string_terminated(buffer, true),
                [0x20..=0x2F, ..] => {
                    self.next_state = Some(State::DcsIntermediate);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn device_control_string_intermediate(
        &mut self,
        buffer: &mut Buffer,
    ) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere_or_escape(buffer)?;

            let mut i = 0;
            while matches!(buffer[i..], [0x20..=0x2F, ..]) {
                i += 1;
            }
            if i != 0 {
                buffer.consume_with(i, |b| self.raise(Token::Intermediate(b)))?;
            }

            let mut i = 0;
            while matches!(buffer[i..], [0x7F | 0x00..=0x17 | 0x19 | 0x1C..=0x1F, ..]) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x40..=0x7E, ..] => self.transition_to_string_terminated(buffer, true),
                [0x30..=0x3F, ..] => {
                    self.next_state = Some(State::DcsIgnore);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }

    fn device_control_string_ignore(
        &mut self,
        buffer: &mut Buffer,
    ) -> Result<(), InternalError<T>> {
        use InternalError::*;
        let mut token = None;
        while buffer.must_advance(&mut token).ok_or_else(|| Unknown)? {
            self.anywhere(buffer)?;

            let mut i = 0;
            while matches!(
                buffer[i..],
                [0x00..=0x17 | 0x19 | 0x1C..=0x1F | 0x20..=0x7F, ..]
            ) {
                i += 1;
            }
            buffer.consume(i);

            match buffer[..] {
                [0x9C, ..] | [0x1B, 0x5C, ..] => {
                    self.consume_escape(buffer);
                    self.next_state = Some(State::Ground);
                    Err(InternalError::SkipToState)
                }
                _ => Ok(()),
            }?;
        }

        Err(InternalError::DataRequired)
    }
}

#[cfg(test)]
mod tests;
