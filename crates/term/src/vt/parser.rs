use std::{
    cell::RefCell,
    ops::{DerefMut, Range},
};

use crate::util::bytes_to_utf8;

use super::{ScannerSink, Token};
use bumpalo::Bump;

static ZERO: &str = "0";

pub trait ParserSink {
    type Error;

    fn substitute(&mut self) -> Result<(), Self::Error>;
    fn ansi(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error>;
    fn dec(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error>;
    fn dcs(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<(), Self::Error>;
    fn esc(&mut self, intermediates: &str, final_char: char) -> Result<(), Self::Error>;
    fn osc(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn pm(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn sos(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn apc(&mut self, data_string: &[u8]) -> Result<(), Self::Error>;
    fn print(&mut self, value: &[u8]) -> Result<(), Self::Error>;
    fn exec(&mut self, function: u8) -> Result<(), Self::Error>;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    Ground,
    Parameters,
    Intermediates,
    Put,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum StringType {
    String,
    PrivacyMessage,
    ApplicationProgramCommand,
    OperatingSystemCommand,
    DeviceControlString(u8),
}

#[derive(Debug, Clone)]
struct StringParser {
    range: Range<usize>,
    ty: StringType,
}

pub struct Parser<T: ParserSink> {
    state: State,
    value: Vec<u8>,
    parameters: Vec<Range<usize>>,
    intermediate: Range<usize>,
    string: Option<StringParser>,
    parameters_arena: Bump,
    sink: RefCell<T>,
}

impl<T: ParserSink + std::fmt::Debug> std::fmt::Debug for Parser<T> {
    #[cfg_attr(coverage_nightly, no_coverage)]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Parser")
            .field("state", &self.state)
            .field("value", &self.value)
            .field("parameters", &self.parameters)
            .field("intermediate", &self.intermediate)
            .field("string", &self.string)
            .field("parameters_arena", &self.parameters_arena)
            .field("sink", &self.sink)
            .finish()
    }
}

impl<T: ParserSink> Parser<T> {
    pub fn new(sink: T) -> Self {
        let result = Self {
            state: State::Ground,
            value: Vec::with_capacity(64),
            parameters: Vec::with_capacity(64),
            intermediate: 0..0,
            string: None,
            parameters_arena: Bump::new(),
            sink: RefCell::new(sink),
        };
        result
    }

    pub fn into_inner(self) -> T {
        self.sink.into_inner()
    }

    fn transition_to_string(&mut self, ty: StringType) {
        let len = self.value.len();
        self.string = Some(StringParser {
            range: len..len,
            ty,
        });
    }

    fn transition_to(&mut self, state: State) {
        if state == self.state && state != State::Ground {
            return;
        }

        tst!(&self.state, &state);

        self.end_state();
        self.state = state;
        match self.state {
            State::Ground => {
                self.string = None;
                self.value.clear();
                self.parameters.clear();
                self.intermediate = 0..0;
                self.string = None;
            }
            State::Parameters => {
                let len = self.value.len();
                self.parameters.clear();
                self.parameters.push(len..len);
            }
            State::Intermediates => {
                let len = self.value.len();
                self.intermediate = len..len;
            }
            _ => {}
        }
    }

    fn end_state(&mut self) {
        match self.state {
            State::Parameters => self.update_parameter(),
            State::Intermediates => self.update_intermediate(),
            State::Put => self.update_put(),
            _ => {}
        }
    }

    #[inline]
    fn dispatch_action<R>(&mut self, mut f: impl FnMut(&mut T) -> R) -> R {
        let r = {
            let mut sink = self.sink.borrow_mut();
            f(sink.deref_mut())
        };
        self.transition_to(State::Ground);
        r
    }

    #[inline]
    fn parameters<R>(&mut self, mut f: impl FnMut(&mut T, &[u8], &str, &[&str]) -> R) -> R {
        let r = {
            let mut params = bumpalo::collections::Vec::with_capacity_in(
                self.parameters.len(),
                &self.parameters_arena,
            );
            let string = &self.value[self
                .string
                .as_ref()
                .map(|v| v.range.clone())
                .unwrap_or(0..0)];
            let intermediate = &self.value[self.intermediate.clone()];
            let mut has_semi = true;
            for param in self.parameters.iter() {
                let range = &self.value[param.clone()];
                let had_semi = std::mem::replace(&mut has_semi, range.last() == Some(&b';'));
                let range = if has_semi {
                    &range[..range.len() - 1]
                } else {
                    &range
                };
                let range = if range.len() == 0 && had_semi {
                    ZERO
                } else {
                    bytes_to_utf8(range)
                };
                params.push(range);
            }
            if params.len() == 0 {
                params.push(ZERO);
            }

            let mut sink = self.sink.borrow_mut();
            f(
                sink.deref_mut(),
                string,
                bytes_to_utf8(intermediate),
                params.as_ref(),
            )
        };
        self.parameters_arena.reset();
        self.transition_to(State::Ground);
        r
    }

    fn put(&mut self, p: &[u8]) -> Result<(), T::Error> {
        self.transition_to(State::Put);
        self.value.extend_from_slice(p);
        self.update_put();
        Ok(())
    }

    fn update_put(&mut self) {
        if let Some(ref mut string) = self.string {
            string.range.end = self.value.len()
        }
    }

    fn param(&mut self, p: &[u8]) -> Result<(), T::Error> {
        self.transition_to(State::Parameters);
        self.value.reserve(p.len());
        for c in p {
            self.value.push(*c);
            if *c == b';' {
                self.update_parameter();
                let i = self.value.len();
                self.parameters.push(i..i);
            }
        }
        Ok(())
    }

    fn update_parameter(&mut self) {
        let mut i = self.value.len();
        let len = self.parameters.len();
        let param = &mut self.parameters[len - 1];
        if param.start == i {
            // According to the spec, an empty parameter is "0"
            self.value.push(b'0');
            i += 1;
        }
        param.end = i;
    }

    fn intermediate(&mut self, p: &[u8]) -> Result<(), T::Error> {
        self.transition_to(State::Intermediates);
        self.value.extend_from_slice(p);
        self.update_intermediate();
        Ok(())
    }

    fn update_intermediate(&mut self) {
        self.intermediate.end = self.value.len()
    }

    fn cs_dispatch(&mut self, c: u8) -> Result<(), T::Error> {
        self.end_state();

        let mut ansi = true;
        if self.parameters.len() != 0 && self.value.len() != 0 && self.value.get(0) == Some(&b'?') {
            self.parameters[0].start += 1;
            ansi = false;
        }

        self.parameters(|sink, _, i, p| {
            if ansi {
                sink.ansi(p, i, c as char)
            } else {
                sink.dec(p, i, c as char)
            }
        })
    }

    fn sos_dispatch(&mut self) -> Result<(), T::Error> {
        self.end_state();
        self.parameters(|sink, s, _, _| sink.sos(s))
    }

    fn pm_dispatch(&mut self) -> Result<(), T::Error> {
        self.end_state();
        self.parameters(|sink, s, _, _| sink.pm(s))
    }

    fn apc_dispatch(&mut self) -> Result<(), T::Error> {
        self.end_state();
        self.parameters(|sink, s, _, _| sink.apc(s))
    }

    fn osc_dispatch(&mut self) -> Result<(), T::Error> {
        self.end_state();
        self.parameters(|sink, s, _, _| sink.osc(s))
    }

    fn dcs_dispatch(&mut self, c: u8) -> Result<(), T::Error> {
        self.end_state();
        self.parameters(|sink, s, i, p| sink.dcs(p, i, c as char, s))
    }

    fn esc_dispatch(&mut self, c: u8) -> Result<(), T::Error> {
        self.end_state();
        self.parameters(|sink, _, i, _| sink.esc(i, c as char))
    }

    fn print_dispatch(&mut self, p: &[u8]) -> Result<(), T::Error> {
        self.end_state();
        self.dispatch_action(|sink| sink.print(p))
    }

    fn execute_dispatch(&mut self, e: u8) -> Result<(), T::Error> {
        self.end_state();
        // Execute is special because it can happen anywhere without causing an
        // return to ground state.
        self.sink.borrow_mut().exec(e)
    }
}

impl<T: ParserSink> ScannerSink for Parser<T> {
    type Error = T::Error;
    fn handle(&mut self, token: Token) -> Result<(), Self::Error> {
        match token {
            Token::Clear => self.transition_to(State::Ground),
            Token::Substitute => self.dispatch_action(|sink| sink.substitute())?,
            Token::Put(p) => self.put(p)?,
            Token::Param(p) => self.param(p)?,
            Token::Intermediate(p) => self.intermediate(p)?,
            Token::Execute(e) => self.execute_dispatch(e)?,
            Token::Print(p) => self.print_dispatch(p)?,
            Token::Escape => self.transition_to(State::Ground),
            Token::EscapeDispatch(c) => self.esc_dispatch(c)?,
            Token::ControlSequence => self.transition_to(State::Ground),
            Token::ControlSequenceDispatch(c) => self.cs_dispatch(c)?,
            Token::DeviceControlString => self.transition_to(State::Ground),
            Token::DeviceControlStringDispatch(c) => {
                self.transition_to_string(StringType::DeviceControlString(c))
            }
            Token::String => self.transition_to_string(StringType::String),
            Token::PrivacyMessage => self.transition_to_string(StringType::PrivacyMessage),
            Token::ApplicationProgramCommand => {
                self.transition_to_string(StringType::ApplicationProgramCommand)
            }
            Token::OperatingSystemCommand => {
                self.transition_to_string(StringType::OperatingSystemCommand)
            }
            Token::End => match &self.string {
                Some(string) => match string.ty {
                    StringType::String => self.sos_dispatch()?,
                    StringType::PrivacyMessage => self.pm_dispatch()?,
                    StringType::ApplicationProgramCommand => self.apc_dispatch()?,
                    StringType::OperatingSystemCommand => self.osc_dispatch()?,
                    StringType::DeviceControlString(c) => self.dcs_dispatch(c)?,
                },
                None => unimplemented!("end should only be encountered after a string type"),
            },
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests;
