use coverage_helper::test;
use std::{cell::RefCell, collections::VecDeque, rc::Rc};

use super::*;
use crate::vt::messages::router::ExecRouter;

#[derive(Debug, Clone)]
struct MessageRecorder<T: Message + PartialEq + Eq>(Rc<RefCell<VecDeque<T>>>);

impl<T: Message + PartialEq + Eq> MessageRecorder<T> {
    fn new() -> Self {
        Self(Rc::new(RefCell::new(VecDeque::new())))
    }

    fn get_call(&mut self) -> T {
        let mut borrowed = self.0.borrow_mut();
        borrowed.pop_front().expect("an additional call")
    }

    fn empty(&self) -> bool {
        self.0.try_borrow().unwrap().len() == 0
    }
}

impl<T: Message + PartialEq + Eq> MessageReceiver<T, ()> for MessageRecorder<T> {
    fn message_received(&mut self, message: T) -> Result<(), ()> {
        let mut borrowed = self.0.borrow_mut();
        borrowed.push_back(message);
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Exec(u8);

impl Message for Exec {
    fn create_filters() -> Vec<MessageFilter> {
        unimplemented!()
    }

    fn try_parse_exec(function: u8) -> Option<Self> {
        Some(Self(function))
    }

    fn write_to<W: crate::vt::MessageWriter>(&self, _: &mut W) -> Result<(), W::Error> {
        unimplemented!()
    }
}

#[test]
fn exec_add_and_execute() {
    let mut router: ExecRouter<()> = ExecRouter::new();
    let mut receiver = MessageRecorder::<Exec>::new();
    let filter = ExecFilter::new()
        .with_function(1)
        .with_function(2)
        .with_function(3);
    router.add(&receiver, &filter);

    // Execute with valid message
    let result = router.execute(1);
    assert_eq!(result, Ok(true));
    assert_eq!(receiver.get_call(), Exec(1));

    // Execute with invalid message
    let result = router.execute(4);
    assert_eq!(result, Ok(false));
    assert!(receiver.empty());

    // Add another receiver and execute with valid message
    let mut receiver2 = MessageRecorder::<Exec>::new();
    router.add(&receiver2, &ExecFilter::new().with_function(5));
    let result = router.execute(5);
    assert_eq!(result, Ok(true));
    assert!(receiver.empty());
    assert_eq!(receiver2.get_call(), Exec(5));
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Print(Vec<u8>);

impl Message for Print {
    fn create_filters() -> Vec<MessageFilter> {
        unimplemented!()
    }

    fn try_parse_print(data_string: &[u8]) -> Option<Self> {
        Some(Self(data_string.to_owned()))
    }

    fn write_to<W: crate::vt::MessageWriter>(&self, _: &mut W) -> Result<(), W::Error> {
        unimplemented!()
    }
}

#[test]
fn print_add_and_execute() {
    let mut router: PrintRouter<()> = PrintRouter::new();
    let mut receiver = MessageRecorder::<Print>::new();
    router.add(&receiver);

    // Execute with valid message
    let result = router.execute(&[1, 2, 3]);
    assert_eq!(result, Ok(true));
    assert_eq!(receiver.get_call(), Print(vec![1, 2, 3]));
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Escape(String, char);

impl Message for Escape {
    fn create_filters() -> Vec<MessageFilter> {
        unimplemented!()
    }

    fn try_parse_esc(intermediates: &str, final_char: char) -> Option<Self> {
        Some(Self(intermediates.to_owned(), final_char))
    }

    fn write_to<W: crate::vt::MessageWriter>(&self, _: &mut W) -> Result<(), W::Error> {
        unimplemented!()
    }
}

#[test]
fn escape_add_and_execute() {
    let mut router: EscapeRouter<()> = EscapeRouter::new();
    let mut receiver = MessageRecorder::<Escape>::new();
    let filter = EscapeFilter::new()
        .with_final('a')
        .with_final('b')
        .with_final('c');
    router.add(&receiver, &filter);

    // Execute with valid message
    let result = router.execute("", 'a');
    assert_eq!(result, Ok(true));
    assert_eq!(receiver.get_call(), Escape("".to_string(), 'a'));

    // Execute with invalid message
    let result = router.execute("", 'd');
    assert_eq!(result, Ok(false));
    assert!(receiver.empty());

    // Add another receiver and execute with valid message
    let mut receiver2 = MessageRecorder::<Escape>::new();
    router.add(&receiver2, &EscapeFilter::new().with_final('e'));
    let result = router.execute("", 'e');
    assert_eq!(result, Ok(true));
    assert!(receiver.empty());
    assert_eq!(receiver2.get_call(), Escape("".to_string(), 'e'));
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct TestString(Vec<u8>);

impl Message for TestString {
    fn create_filters() -> Vec<MessageFilter> {
        unimplemented!()
    }

    fn try_parse_sos(data_string: &[u8]) -> Option<Self> {
        Some(Self(data_string.to_owned()))
    }

    fn write_to<W: crate::vt::MessageWriter>(&self, _: &mut W) -> Result<(), W::Error> {
        unimplemented!()
    }
}

#[test]
fn string_add_and_execute() {
    let mut router: StringRouter<()> = StringRouter::new();
    let mut receiver = MessageRecorder::<TestString>::new();
    let filter = StringFilter::new()
        .with_first_param(b"100")
        .with_first_param(b"200")
        .with_first_param(b"300");
    router.add(&receiver, &filter, TestString::try_parse_sos);

    // Execute with valid message
    let result = router.execute(b"100;test");
    assert_eq!(result, Ok(true));
    assert_eq!(receiver.get_call(), TestString(b"100;test".to_vec()));

    // Execute with valid message
    let result = router.execute(b"200;test");
    assert_eq!(result, Ok(true));
    assert_eq!(receiver.get_call(), TestString(b"200;test".to_vec()));

    // Execute with invalid message
    let result = router.execute(b"100test");
    assert_eq!(result, Ok(false));
    assert!(receiver.empty());

    // Add another receiver and execute with valid message
    let mut receiver2 = MessageRecorder::<TestString>::new();
    router.add(
        &receiver2,
        &StringFilter::new().with_first_param(b"1"),
        TestString::try_parse_sos,
    );
    let result = router.execute(b"1;foo");
    assert_eq!(result, Ok(true));
    assert!(receiver.empty());
    assert_eq!(receiver2.get_call(), TestString(b"1;foo".to_vec()));
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct ControlSequence(Vec<String>, String, char);

impl Message for ControlSequence {
    fn create_filters() -> Vec<MessageFilter> {
        unimplemented!()
    }

    fn try_parse_ansi(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        Some(Self(
            parameters.iter().map(|s| s.to_string()).collect(),
            intermediates.to_owned(),
            final_char,
        ))
    }

    fn write_to<W: crate::vt::MessageWriter>(&self, _: &mut W) -> Result<(), W::Error> {
        unimplemented!()
    }
}

#[test]
fn control_sequence_add_and_execute() {
    let mut router: ControlSequenceRouter<()> = ControlSequenceRouter::new();
    let mut receiver = MessageRecorder::new();
    let filter = ControlSequenceFilter::new()
        .with_final('a')
        .with_final('b')
        .with_final('c');
    router.add(&receiver, &filter, ControlSequence::try_parse_ansi);

    // Execute with valid message
    let result = router.execute(&["1"], "some", 'a');
    assert_eq!(result, Ok(true));
    assert_eq!(
        receiver.get_call(),
        ControlSequence(vec!["1".to_string()], "some".to_string(), 'a')
    );

    // Execute with invalid message
    let result = router.execute(&["1"], "some", 'd');
    assert_eq!(result, Ok(false));
    assert!(receiver.empty());

    // Add another receiver and execute with valid message
    let mut receiver2 = MessageRecorder::<ControlSequence>::new();
    router.add(
        &receiver2,
        &ControlSequenceFilter::new().with_final('e'),
        ControlSequence::try_parse_ansi,
    );
    let result = router.execute(&["1"], "some", 'e');
    assert_eq!(result, Ok(true));
    assert!(receiver.empty());
    assert_eq!(
        receiver2.get_call(),
        ControlSequence(vec!["1".to_string()], "some".to_string(), 'e')
    );
}
