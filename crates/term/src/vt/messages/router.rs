use std::{
    collections::HashMap,
    ops::{Bound, RangeBounds},
};

use crate::vt::ParserSink;

use super::Message;

/// Allows filtering of VT messages that contain parameters, intermediates, and
/// a final character.
#[derive(Debug)]
pub struct ControlSequenceFilter {
    param_count: Vec<(Bound<u8>, Bound<u8>)>,
    first_param: Vec<&'static str>,
    intermediates: Vec<&'static str>,
    final_char: Vec<char>,
}

impl ControlSequenceFilter {
    pub fn new() -> Self {
        Self {
            param_count: Vec::new(),
            first_param: Vec::new(),
            intermediates: Vec::new(),
            final_char: Vec::new(),
        }
    }

    /// Indicates that a range of parameter counts are supported.
    pub fn with_param_count(mut self, v: impl RangeBounds<u8>) -> Self {
        let start = match v.start_bound() {
            Bound::Included(a) => Bound::Included(*a),
            Bound::Excluded(b) => Bound::Excluded(*b),
            Bound::Unbounded => Bound::Unbounded,
        };
        let end = match v.end_bound() {
            Bound::Included(a) => Bound::Included(*a),
            Bound::Excluded(b) => Bound::Excluded(*b),
            Bound::Unbounded => Bound::Unbounded,
        };
        self.param_count.push((start, end));
        self
    }

    /// Indicates that a specific first parameter is required.
    pub fn with_first_param(mut self, v: &'static str) -> Self {
        self.first_param.push(v);
        self
    }

    /// Indicates that a specific value for intermediate characters is required.
    pub fn with_intermediates(mut self, v: &'static str) -> Self {
        self.intermediates.push(v);
        self
    }

    /// Indicates that a specific final character is required.
    pub fn with_final(mut self, v: char) -> Self {
        self.final_char.push(v);
        self
    }

    pub fn param_count(&self) -> &[(Bound<u8>, Bound<u8>)] {
        self.param_count.as_ref()
    }

    pub fn first_param(&self) -> &[&str] {
        self.first_param.as_ref()
    }

    pub fn intermediates(&self) -> &[&str] {
        self.intermediates.as_ref()
    }

    pub fn final_char(&self) -> &[char] {
        self.final_char.as_ref()
    }
}

/// Allows filtering of VT messags that contain a data string only.
#[derive(Debug)]
pub struct StringFilter {
    first_param: Vec<&'static [u8]>,
}

impl StringFilter {
    pub fn new() -> Self {
        Self {
            first_param: Vec::new(),
        }
    }

    /// Indicates that the data string must be prefixes with the specified
    /// value and a semicolon, which is a common convention for data string
    /// VT messages.
    pub fn with_first_param(mut self, v: &'static [u8]) -> Self {
        self.first_param.push(v);
        self
    }

    pub fn first_param<'b>(&'b self) -> &'b [&'static [u8]] {
        self.first_param.as_ref()
    }
}

/// Allows filtering of VT messages that contain intermediates, and a final
/// character.
#[derive(Debug)]
pub struct EscapeFilter {
    intermediates: Vec<&'static str>,
    final_char: Vec<char>,
}

impl EscapeFilter {
    pub fn new() -> Self {
        Self {
            intermediates: Vec::new(),
            final_char: Vec::new(),
        }
    }

    /// Indicates that a specific value for intermediate characters is required.
    pub fn with_intermediates(mut self, v: &'static str) -> Self {
        self.intermediates.push(v);
        self
    }

    /// Indicates that a specific final character is required.
    pub fn with_final(mut self, v: char) -> Self {
        self.final_char.push(v);
        self
    }

    pub fn intermediates(&self) -> &[&str] {
        self.intermediates.as_ref()
    }

    pub fn final_char(&self) -> &[char] {
        self.final_char.as_ref()
    }
}

/// Allows filtering of VT messages that consist of a single character.
#[derive(Debug)]
pub struct ExecFilter {
    function: Vec<u8>,
}

impl ExecFilter {
    pub fn new() -> Self {
        Self {
            function: Vec::new(),
        }
    }

    /// Indicates the function that is expected.
    pub fn with_function(mut self, v: u8) -> Self {
        self.function.push(v);
        self
    }

    pub fn function(&self) -> &[u8] {
        self.function.as_ref()
    }
}

/// Indicates how a parser is selected for a VT message.
#[derive(Debug)]
pub enum MessageFilter {
    Ansi(ControlSequenceFilter),
    Dec(ControlSequenceFilter),
    Esc(EscapeFilter),
    Dcs(ControlSequenceFilter),
    Exec(ExecFilter),
    Pm(StringFilter),
    Osc(StringFilter),
    Sos(StringFilter),
    Apc(StringFilter),
    Print,
}

impl MessageFilter {
    /// Creates a new filter for ANSI VT messages (control sequences that do not
    /// begin with '?').
    pub fn ansi(f: impl FnOnce(ControlSequenceFilter) -> ControlSequenceFilter) -> Self {
        let cs = ControlSequenceFilter::new();
        Self::Ansi(f(cs))
    }

    /// Creates a new filter for DEC VT messages (control sequences that do
    /// begin with '?').
    pub fn dec(f: impl FnOnce(ControlSequenceFilter) -> ControlSequenceFilter) -> Self {
        let cs = ControlSequenceFilter::new();
        Self::Dec(f(cs))
    }

    /// Creates a new filter for device control string VT messages.
    pub fn dcs(f: impl FnOnce(ControlSequenceFilter) -> ControlSequenceFilter) -> Self {
        let cs = ControlSequenceFilter::new();
        Self::Dcs(f(cs))
    }

    /// Creates a new filter for print messages.
    pub fn print() -> Self {
        Self::Print
    }

    /// Creates a new filter for escape VT messages.
    pub fn esc(f: impl FnOnce(EscapeFilter) -> EscapeFilter) -> Self {
        let cs = EscapeFilter::new();
        Self::Esc(f(cs))
    }

    /// Creates a new filter for execute VT messages.
    pub fn exec(f: impl FnOnce(ExecFilter) -> ExecFilter) -> Self {
        let cs = ExecFilter::new();
        Self::Exec(f(cs))
    }

    /// Creates a new filter for privacy message VT messages.
    pub fn pm(f: impl FnOnce(StringFilter) -> StringFilter) -> Self {
        let cs = StringFilter::new();
        Self::Pm(f(cs))
    }

    /// Creates a new filter for string VT messages.
    pub fn sos(f: impl FnOnce(StringFilter) -> StringFilter) -> Self {
        let cs = StringFilter::new();
        Self::Sos(f(cs))
    }

    /// Creates a new filter for operating system command VT messages.
    pub fn osc(f: impl FnOnce(StringFilter) -> StringFilter) -> Self {
        let cs = StringFilter::new();
        Self::Osc(f(cs))
    }

    /// Creates a new filter for application program command VT messages.
    pub fn apc(f: impl FnOnce(StringFilter) -> StringFilter) -> Self {
        let cs = StringFilter::new();
        Self::Apc(f(cs))
    }
}

struct DeviceControlStringRouter<E> {
    routes: HashMap<char, Vec<Box<dyn FnMut(&[&str], &str, char, &[u8]) -> Result<bool, E>>>>,
}

impl<E> DeviceControlStringRouter<E> {
    fn new() -> Self {
        Self {
            routes: HashMap::new(),
        }
    }

    fn add<T: Message, R>(&mut self, receiver: &R, filter: &ControlSequenceFilter)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
    {
        for c in filter.final_char() {
            let mut receiver = receiver.clone();
            let f: Box<dyn FnMut(&[&str], &str, char, &[u8]) -> Result<bool, E>> =
                Box::new(move |parameters, intermediates, final_char, data_string| {
                    if let Some(msg) =
                        T::try_parse_dcs(parameters, intermediates, final_char, data_string)
                    {
                        receiver.message_received(msg)?;
                        Ok(true)
                    } else {
                        Ok(false)
                    }
                });
            self.routes.entry(*c).or_default().push(f)
        }
    }

    fn execute(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<bool, E> {
        if let Some(parsers) = self.routes.get_mut(&final_char) {
            for parser in parsers.iter_mut() {
                if parser(parameters, intermediates, final_char, data_string)? {
                    return Ok(true);
                }
            }
        }
        Ok(false)
    }
}

struct StringRouter<E> {
    routes: HashMap<&'static [u8], Vec<Box<dyn FnMut(&[u8]) -> Result<bool, E>>>>,
}

impl<E> StringRouter<E> {
    fn new() -> Self {
        Self {
            routes: HashMap::new(),
        }
    }

    fn add<T, R, F>(&mut self, receiver: &R, filter: &StringFilter, try_parse: F)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
        T: Message,
        F: FnMut(&[u8]) -> Option<T> + Clone + 'static,
    {
        for c in filter.first_param() {
            let mut receiver = receiver.clone();
            let mut try_parse = try_parse.clone();
            let f: Box<dyn FnMut(&[u8]) -> Result<bool, E>> = Box::new(move |data_string| {
                if let Some(msg) = try_parse(data_string) {
                    receiver.message_received(msg)?;
                    Ok(true)
                } else {
                    Ok(false)
                }
            });
            self.routes.entry(c).or_default().push(f)
        }
    }

    fn execute(&mut self, data_string: &[u8]) -> Result<bool, E> {
        if let Some(index) = data_string.iter().position(|&v| v == b';') {
            let sub = &data_string[..index];
            if let Some(parsers) = self.routes.get_mut(sub) {
                for parser in parsers.iter_mut() {
                    if parser(data_string)? {
                        return Ok(true);
                    }
                }
            }
        }
        Ok(false)
    }
}

struct ControlSequenceRouter<E> {
    routes: HashMap<char, Vec<Box<dyn FnMut(&[&str], &str, char) -> Result<bool, E>>>>,
}

impl<E> ControlSequenceRouter<E> {
    fn new() -> Self {
        Self {
            routes: HashMap::new(),
        }
    }

    fn add<T: Message, R, F>(&mut self, receiver: &R, filter: &ControlSequenceFilter, try_parse: F)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
        F: FnMut(&[&str], &str, char) -> Option<T> + Clone + 'static,
    {
        for c in filter.final_char() {
            let mut receiver = receiver.clone();
            let mut try_parse = try_parse.clone();
            let f: Box<dyn FnMut(&[&str], &str, char) -> Result<bool, E>> =
                Box::new(move |parameters, intermediates, final_char| {
                    if let Some(msg) = try_parse(parameters, intermediates, final_char) {
                        receiver.message_received(msg)?;
                        Ok(true)
                    } else {
                        Ok(false)
                    }
                });
            self.routes.entry(*c).or_default().push(f)
        }
    }

    fn execute(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<bool, E> {
        if let Some(parsers) = self.routes.get_mut(&final_char) {
            for parser in parsers.iter_mut() {
                if parser(parameters, intermediates, final_char)? {
                    return Ok(true);
                }
            }
        }
        Ok(false)
    }
}

struct EscapeRouter<E> {
    routes: HashMap<char, Vec<Box<dyn FnMut(&str, char) -> Result<bool, E>>>>,
}

impl<E> EscapeRouter<E> {
    fn new() -> Self {
        Self {
            routes: HashMap::new(),
        }
    }

    fn add<T: Message, R>(&mut self, receiver: &R, filter: &EscapeFilter)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
    {
        for c in filter.final_char() {
            let mut receiver = receiver.clone();
            let f: Box<dyn FnMut(&str, char) -> Result<bool, E>> =
                Box::new(move |intermediates, final_char| {
                    if let Some(msg) = T::try_parse_esc(intermediates, final_char) {
                        receiver.message_received(msg)?;
                        Ok(true)
                    } else {
                        Ok(false)
                    }
                });
            self.routes.entry(*c).or_default().push(f)
        }
    }

    fn execute(&mut self, intermediates: &str, final_char: char) -> Result<bool, E> {
        if let Some(parsers) = self.routes.get_mut(&final_char) {
            for parser in parsers.iter_mut() {
                if parser(intermediates, final_char)? {
                    return Ok(true);
                }
            }
        }
        Ok(false)
    }
}

struct PrintRouter<E> {
    routes: Vec<Box<dyn FnMut(&[u8]) -> Result<bool, E>>>,
}

impl<E> PrintRouter<E> {
    fn new() -> Self {
        Self { routes: Vec::new() }
    }

    fn add<T: Message, R>(&mut self, receiver: &R)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
    {
        let mut receiver = receiver.clone();
        let f: Box<dyn FnMut(&[u8]) -> Result<bool, E>> = Box::new(move |val| {
            if let Some(msg) = T::try_parse_print(val) {
                receiver.message_received(msg)?;
                Ok(true)
            } else {
                Ok(false)
            }
        });
        self.routes.push(f)
    }

    fn execute(&mut self, value: &[u8]) -> Result<bool, E> {
        for parser in self.routes.iter_mut() {
            if parser(value)? {
                return Ok(true);
            }
        }

        Ok(false)
    }
}

struct ExecRouter<E> {
    routes: HashMap<u8, Vec<Box<dyn FnMut(u8) -> Result<bool, E>>>>,
}

impl<E> ExecRouter<E> {
    fn new() -> Self {
        Self {
            routes: HashMap::new(),
        }
    }

    fn add<T: Message, R>(&mut self, receiver: &R, filter: &ExecFilter)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
    {
        for c in filter.function() {
            let mut receiver = receiver.clone();
            let f: Box<dyn FnMut(u8) -> Result<bool, E>> = Box::new(move |function| {
                if let Some(msg) = T::try_parse_exec(function) {
                    receiver.message_received(msg)?;
                    Ok(true)
                } else {
                    Ok(false)
                }
            });
            self.routes.entry(*c).or_default().push(f)
        }
    }

    fn execute(&mut self, function: u8) -> Result<bool, E> {
        if let Some(parsers) = self.routes.get_mut(&function) {
            for parser in parsers.iter_mut() {
                if parser(function)? {
                    return Ok(true);
                }
            }
        }

        Ok(false)
    }
}

/// Allows  [`Message`] types to be registered and router to a
/// [`MessageReceiver`].
pub struct MessageRouter<E = ()> {
    ansi_routes: ControlSequenceRouter<E>,
    dec_routes: ControlSequenceRouter<E>,
    dcs_routes: DeviceControlStringRouter<E>,
    esc_routes: EscapeRouter<E>,
    exec_routes: ExecRouter<E>,
    print_routes: PrintRouter<E>,
    pm_routes: StringRouter<E>,
    sos_routes: StringRouter<E>,
    osc_routes: StringRouter<E>,
    apc_routes: StringRouter<E>,
}

pub trait MessageReceiver<T: Message, E = ()> {
    fn message_received(&mut self, message: T) -> Result<(), E>;
}

impl<T, E, F> MessageReceiver<T, E> for F
where
    T: Message,
    F: FnMut(T) -> Result<(), E>,
{
    fn message_received(&mut self, message: T) -> Result<(), E> {
        self(message)
    }
}

impl<E: std::fmt::Debug> MessageRouter<E> {
    pub fn new() -> Self {
        Self {
            ansi_routes: ControlSequenceRouter::new(),
            dec_routes: ControlSequenceRouter::new(),
            dcs_routes: DeviceControlStringRouter::new(),
            esc_routes: EscapeRouter::new(),
            exec_routes: ExecRouter::new(),
            print_routes: PrintRouter::new(),
            pm_routes: StringRouter::new(),
            sos_routes: StringRouter::new(),
            osc_routes: StringRouter::new(),
            apc_routes: StringRouter::new(),
        }
    }

    pub fn register<T: Message + 'static, R>(&mut self, receiver: R)
    where
        R: MessageReceiver<T, E> + Clone + 'static,
    {
        let receiver = &receiver;
        for filter in T::create_filters() {
            match filter {
                MessageFilter::Ansi(filter) => {
                    self.ansi_routes.add(receiver, &filter, T::try_parse_ansi)
                }
                MessageFilter::Dec(filter) => {
                    self.dec_routes.add(receiver, &filter, T::try_parse_dec)
                }
                MessageFilter::Dcs(filter) => self.dcs_routes.add(receiver, &filter),
                MessageFilter::Esc(filter) => self.esc_routes.add(receiver, &filter),
                MessageFilter::Exec(filter) => self.exec_routes.add(receiver, &filter),
                MessageFilter::Print => self.print_routes.add(receiver),
                MessageFilter::Pm(filter) => self.pm_routes.add(receiver, &filter, T::try_parse_pm),
                MessageFilter::Apc(filter) => {
                    self.apc_routes.add(receiver, &filter, T::try_parse_apc)
                }
                MessageFilter::Sos(filter) => {
                    self.sos_routes.add(receiver, &filter, T::try_parse_sos)
                }
                MessageFilter::Osc(filter) => {
                    self.osc_routes.add(receiver, &filter, T::try_parse_sos)
                }
            }
        }
    }
}

impl<E> ParserSink for MessageRouter<E> {
    type Error = E;

    fn substitute(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn ansi(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        self.ansi_routes
            .execute(parameters, intermediates, final_char)?;
        Ok(())
    }

    fn dec(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        self.dec_routes
            .execute(parameters, intermediates, final_char)?;
        Ok(())
    }

    fn dcs(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<(), Self::Error> {
        self.dcs_routes
            .execute(parameters, intermediates, final_char, data_string)?;
        Ok(())
    }

    fn print(&mut self, value: &[u8]) -> Result<(), Self::Error> {
        self.print_routes.execute(value)?;
        Ok(())
    }

    fn esc(&mut self, intermediates: &str, final_char: char) -> Result<(), Self::Error> {
        self.esc_routes.execute(intermediates, final_char)?;
        Ok(())
    }

    fn exec(&mut self, function: u8) -> Result<(), Self::Error> {
        self.exec_routes.execute(function)?;
        Ok(())
    }

    fn osc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.osc_routes.execute(data_string)?;
        Ok(())
    }

    fn pm(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.pm_routes.execute(data_string)?;
        Ok(())
    }

    fn sos(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.sos_routes.execute(data_string)?;
        Ok(())
    }

    fn apc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.apc_routes.execute(data_string)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests;
