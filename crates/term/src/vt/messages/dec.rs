use std::{
    collections::HashSet,
    ops::{Deref, DerefMut},
};

use crate::{util::IntoNumber, vt::Version};

use super::{
    Message, MessageFilter, MessageWriter, ModeSetting, PrimaryDeviceAttribute, MAX_PARAMETERS,
};
use num_enum::{FromPrimitive, IntoPrimitive, TryFromPrimitive};
use tinyvec::tiny_vec;

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone, FromPrimitive, IntoPrimitive)]
#[repr(u16)]
pub enum DecMode {
    AlternateScreen = 1047,
    SaveCursor = 1048,
    AlternateScreenWithSaveCursor = 1049,
    SynchronizedOutput = 2026,
    #[num_enum(catch_all)]
    Unknown(u16),
}

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone)]
pub struct DecModeRequest(DecMode);

impl DecModeRequest {
    pub fn new(mode: DecMode) -> Self {
        Self(mode)
    }
}

impl Message for DecModeRequest {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::dec(|c| {
            c.with_param_count(1..=1)
                .with_intermediates("$")
                .with_final('p')
        })]
    }

    fn try_parse_dec(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "$" && final_char == 'p' && parameters.len() == 1 {
            parameters[0]
                .parse()
                .ok()
                .map(|v: u16| DecModeRequest(v.into()))
        } else {
            None
        }
    }

    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        writer.write_dec(&[self.0.into_number()], "$", 'p')
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DecModeResponse {
    pub mode: DecMode,
    pub setting: ModeSetting,
}

impl Message for DecModeResponse {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::dec(|c| {
            c.with_param_count(2..=2)
                .with_intermediates("$")
                .with_final('y')
        })]
    }

    fn try_parse_dec(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "$" && final_char == 'y' && parameters.len() == 2 {
            let setting = parameters[1]
                .parse()
                .ok()
                .and_then(|x| ModeSetting::try_from_primitive(x).ok())?;
            let mode = parameters[0].parse().ok().map(DecMode::from_primitive)?;
            Some(Self { mode, setting })
        } else {
            None
        }
    }

    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        writer.write_dec(
            &[self.mode.into_number(), self.setting.into_number()],
            "$",
            'y',
        )
    }
}

#[derive(Debug, Clone)]
pub struct DecModeSet {
    modes: HashSet<DecMode>,
}

impl DecModeSet {
    pub fn single(v: DecMode) -> Self {
        let mut modes = HashSet::with_capacity(1);
        modes.insert(v);
        Self { modes }
    }
}

impl Default for DecModeSet {
    fn default() -> Self {
        Self {
            modes: Default::default(),
        }
    }
}

impl FromIterator<DecMode> for DecModeSet {
    fn from_iter<T: IntoIterator<Item = DecMode>>(iter: T) -> Self {
        Self {
            modes: HashSet::from_iter(iter),
        }
    }
}

impl<T: AsRef<[DecMode]>> From<T> for DecModeSet {
    fn from(value: T) -> Self {
        value.as_ref().iter().cloned().collect()
    }
}

impl Deref for DecModeSet {
    type Target = HashSet<DecMode>;

    fn deref(&self) -> &Self::Target {
        &self.modes
    }
}

impl DerefMut for DecModeSet {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.modes
    }
}

impl Message for DecModeSet {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::dec(|c| {
            c.with_param_count(1..)
                .with_intermediates("")
                .with_final('h')
        })]
    }

    fn try_parse_dec(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if parameters.len() != 0 && intermediates == "" && final_char == 'h' {
            let mut modes = HashSet::with_capacity(parameters.len());
            for param in parameters {
                let mode = param
                    .parse()
                    .ok()
                    .and_then(|x| DecMode::try_from_primitive(x).ok())?;
                modes.insert(mode);
            }
            Some(Self { modes })
        } else {
            None
        }
    }

    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        let mut parameters = tiny_vec!([&str; MAX_PARAMETERS]);
        self.modes
            .iter()
            .map(|v| v.into_number())
            .for_each(|v| parameters.push(v));
        writer.write_dec(parameters.as_ref(), "", 'h')
    }
}

#[derive(Debug, Clone)]
pub struct DecModeReset {
    modes: HashSet<DecMode>,
}

impl DecModeReset {
    pub fn single(v: DecMode) -> Self {
        let mut modes = HashSet::with_capacity(1);
        modes.insert(v);
        Self { modes }
    }
}

impl Default for DecModeReset {
    fn default() -> Self {
        Self {
            modes: Default::default(),
        }
    }
}

impl FromIterator<DecMode> for DecModeReset {
    fn from_iter<T: IntoIterator<Item = DecMode>>(iter: T) -> Self {
        Self {
            modes: HashSet::from_iter(iter),
        }
    }
}

impl<T: AsRef<[DecMode]>> From<T> for DecModeReset {
    fn from(value: T) -> Self {
        value.as_ref().iter().cloned().collect()
    }
}

impl Deref for DecModeReset {
    type Target = HashSet<DecMode>;

    fn deref(&self) -> &Self::Target {
        &self.modes
    }
}

impl DerefMut for DecModeReset {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.modes
    }
}

impl Message for DecModeReset {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::dec(|c| {
            c.with_param_count(1..)
                .with_intermediates("")
                .with_final('l')
        })]
    }

    fn try_parse_dec(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if parameters.len() != 0 && intermediates == "" && final_char == 'l' {
            let mut modes = HashSet::with_capacity(parameters.len());
            for param in parameters {
                let mode = param
                    .parse()
                    .ok()
                    .and_then(|x| DecMode::try_from_primitive(x).ok())?;
                modes.insert(mode);
            }
            Some(Self { modes })
        } else {
            None
        }
    }

    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        let mut parameters = tiny_vec!([&str; MAX_PARAMETERS]);
        self.modes
            .iter()
            .map(|v| v.into_number())
            .for_each(|v| parameters.push(v));
        writer.write_dec(parameters.as_ref(), "", 'l')
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DeviceAttributesResponse {
    pub version: Version,
    attributes: HashSet<PrimaryDeviceAttribute>,
}

impl Deref for DeviceAttributesResponse {
    type Target = HashSet<PrimaryDeviceAttribute>;

    fn deref(&self) -> &Self::Target {
        &self.attributes
    }
}

impl DerefMut for DeviceAttributesResponse {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.attributes
    }
}

impl Message for DeviceAttributesResponse {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::dec(|c| {
            c.with_param_count(1..)
                .with_final('c')
                .with_intermediates("")
        })]
    }

    fn try_parse_dec(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "" && final_char == 'c' && parameters.len() >= 1 {
            let mut attributes = HashSet::new();
            for param in parameters.iter().skip(1) {
                let v: u16 = param.parse().ok()?;
                attributes.insert(v.into());
            }
            let version = parameters[0].parse::<u16>().ok()?.into();
            Some(Self {
                version,
                attributes,
            })
        } else {
            None
        }
    }

    fn write_to<W: MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        let mut parameters = tiny_vec!([&str; MAX_PARAMETERS]);
        parameters.push(self.version.into_number());
        self.attributes
            .iter()
            .map(|v| v.into_number())
            .for_each(|v| parameters.push(v));
        writer.write_dec(parameters.as_ref(), "", 'c')
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::vt::messages::test_writer::TestWriter;

    #[test]
    fn dec_mode_write_to() {
        let mode = DecMode::AlternateScreen;
        let request = DecModeRequest::new(mode);
        let mut writer = TestWriter::new();
        request.write_to(&mut writer).unwrap();
        writer.assert_dec_call(&["1047"], "$", 'p');
    }

    #[test]
    fn dec_mode_request_try_parse_dec() {
        let expected = Some(DecModeRequest(DecMode::AlternateScreen));
        let result = DecModeRequest::try_parse_dec(&["1047"], "$", 'p');
        assert_eq!(result, expected);

        let expected = Some(DecModeRequest(DecMode::Unknown(500)));
        let result = DecModeRequest::try_parse_dec(&["500"], "$", 'p');
        assert_eq!(result, expected);

        let result = DecModeRequest::try_parse_dec(&["1047", "1048"], "$", 'p');
        assert_eq!(result, None);

        let result = DecModeRequest::try_parse_dec(&["1047"], "#", 'p');
        assert_eq!(result, None);

        let result = DecModeRequest::try_parse_dec(&["1047"], "$", 'q');
        assert_eq!(result, None);

        let result = DecModeRequest::try_parse_dec(&["abc"], "$", 'p');
        assert_eq!(result, None);
    }

    #[test]
    fn dec_mode_set_try_parse_dec() {
        let mode_set = DecModeSet::try_parse_dec(&["1047"], "", 'h').unwrap();
        assert_eq!(mode_set.modes.len(), 1);
        assert!(mode_set.modes.contains(&DecMode::AlternateScreen));

        let mode_set = DecModeSet::try_parse_dec(&["1047", "1048"], "", 'h').unwrap();
        assert_eq!(mode_set.modes.len(), 2);
        assert!(mode_set.modes.contains(&DecMode::AlternateScreen));
        assert!(mode_set.modes.contains(&DecMode::SaveCursor));

        let mode_set = DecModeSet::try_parse_dec(&["1047", "1048"], "foo", 'h');
        assert!(mode_set.is_none());
    }

    #[test]
    fn dec_mode_set_write_to() {
        let mut writer = TestWriter::new();
        let mode_set = DecModeSet::from_iter(vec![
            DecMode::AlternateScreen,
            DecMode::SynchronizedOutput,
            DecMode::SaveCursor,
        ]);
        mode_set.write_to(&mut writer).unwrap();

        let call = writer.get_dec_call().unwrap();
        assert_set_eq!(call.0.iter(), ["1047", "1048", "2026"].iter());
        assert_eq!(call.1, "");
        assert_eq!(call.2, 'h');
    }

    #[test]
    fn dec_mode_set_single() {
        let dec_mode_set = DecModeSet::single(DecMode::AlternateScreen);
        let mut writer = TestWriter::new();
        dec_mode_set.write_to(&mut writer).unwrap();
        writer.assert_dec_call(&["1047"], "", 'h');
    }

    #[test]
    fn dec_mode_set_from_iterator() {
        let dec_mode_set: DecModeSet = vec![DecMode::AlternateScreen, DecMode::SynchronizedOutput]
            .into_iter()
            .collect();
        let mut writer = TestWriter::new();
        dec_mode_set.write_to(&mut writer).unwrap();

        let call = writer.get_dec_call().unwrap();
        assert_set_eq!(call.0.iter(), ["1047", "2026"].iter());
        assert_eq!(call.1, "");
        assert_eq!(call.2, 'h');
    }

    #[test]
    fn dec_mode_reset_try_parse_dec() {
        let mode_set = DecModeReset::try_parse_dec(&["1047"], "", 'l').unwrap();
        assert_eq!(mode_set.modes.len(), 1);
        assert!(mode_set.modes.contains(&DecMode::AlternateScreen));

        let mode_set = DecModeReset::try_parse_dec(&["1047", "1048"], "", 'l').unwrap();
        assert_eq!(mode_set.modes.len(), 2);
        assert!(mode_set.modes.contains(&DecMode::AlternateScreen));
        assert!(mode_set.modes.contains(&DecMode::SaveCursor));

        let mode_set = DecModeReset::try_parse_dec(&["1047", "1048"], "foo", 'l');
        assert!(mode_set.is_none());
    }

    #[test]
    fn dec_mode_reset_write_to() {
        let mut writer = TestWriter::new();
        let mode_set = DecModeReset::from_iter(vec![
            DecMode::AlternateScreen,
            DecMode::SynchronizedOutput,
            DecMode::SaveCursor,
        ]);
        mode_set.write_to(&mut writer).unwrap();

        let call = writer.get_dec_call().unwrap();
        assert_set_eq!(call.0.iter(), ["1047", "1048", "2026"].iter());
        assert_eq!(call.1, "");
        assert_eq!(call.2, 'l');
    }

    #[test]
    fn dec_mode_reset_single() {
        let dec_mode_set = DecModeReset::single(DecMode::AlternateScreen);
        let mut writer = TestWriter::new();
        dec_mode_set.write_to(&mut writer).unwrap();
        writer.assert_dec_call(&["1047"], "", 'l');
    }

    #[test]
    fn dec_mode_reset_from_iterator() {
        let dec_mode_set: DecModeReset =
            vec![DecMode::AlternateScreen, DecMode::SynchronizedOutput]
                .into_iter()
                .collect();
        let mut writer = TestWriter::new();
        dec_mode_set.write_to(&mut writer).unwrap();

        let call = writer.get_dec_call().unwrap();
        assert_set_eq!(call.0.iter(), ["1047", "2026"].iter());
        assert_eq!(call.1, "");
        assert_eq!(call.2, 'l');
    }

    #[test]
    fn device_attributes_response_try_parse_dec() {
        let result = DeviceAttributesResponse::try_parse_dec(&["1", "4", "44", "69"], "", 'c');
        assert_eq!(
            result,
            Some(DeviceAttributesResponse {
                version: Version::Vt100,
                attributes: [
                    PrimaryDeviceAttribute::Sixel,
                    PrimaryDeviceAttribute::PcTerm,
                    PrimaryDeviceAttribute::Unknown(69),
                ]
                .iter()
                .cloned()
                .collect(),
            })
        );

        let result = DeviceAttributesResponse::try_parse_dec(&["63"], "", 'c');
        assert_eq!(
            result,
            Some(DeviceAttributesResponse {
                version: Version::Vt320,
                attributes: HashSet::new()
            })
        );
    }

    #[test]
    fn device_attributes_response_write_to() {
        let response = DeviceAttributesResponse {
            version: Version::Vt100,
            attributes: [
                PrimaryDeviceAttribute::Sixel,
                PrimaryDeviceAttribute::PcTerm,
                PrimaryDeviceAttribute::Unknown(69),
            ]
            .iter()
            .cloned()
            .collect(),
        };
        let mut writer = TestWriter::new();
        response.write_to(&mut writer).unwrap();

        let call = writer.get_dec_call().unwrap();
        assert_set_eq!(call.0.iter().skip(1), ["4", "44", "69"].iter());
        assert_eq!(call.1, "");
        assert_eq!(call.2, 'c');
    }

    #[test]
    fn dec_mode_response_try_parse_dec() {
        let dec_mode_response =
            DecModeResponse::try_parse_dec(&(vec!["1047", "1"]), "$", 'y').unwrap();
        let expected_dec_mode_response = DecModeResponse {
            mode: DecMode::AlternateScreen,
            setting: ModeSetting::Set,
        };
        assert_eq!(dec_mode_response, expected_dec_mode_response);

        let dec_mode_response = DecModeResponse::try_parse_dec(&(vec!["1047", "1"]), "?", 'y');
        assert_eq!(dec_mode_response, None);
    }

    #[test]
    fn dec_mode_response_write_to() {
        let mut writer = TestWriter::new();
        (DecModeResponse {
            mode: DecMode::AlternateScreen,
            setting: ModeSetting::Set,
        })
        .write_to(&mut writer)
        .unwrap();

        (DecModeResponse {
            mode: DecMode::AlternateScreen,
            setting: ModeSetting::Reset,
        })
        .write_to(&mut writer)
        .unwrap();

        writer.assert_dec_call(&["1047", "1"], "$", 'y');
        writer.assert_dec_call(&["1047", "2"], "$", 'y');
    }
}
