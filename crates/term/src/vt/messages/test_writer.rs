use super::MessageWriter;

use std::collections::VecDeque;

pub struct TestWriter {
    ansi_calls: VecDeque<(Vec<String>, String, char)>,
    dec_calls: VecDeque<(Vec<String>, String, char)>,
    dcs_calls: VecDeque<(Vec<String>, String, char, Vec<u8>)>,
    esc_calls: VecDeque<(String, char)>,
    osc_calls: VecDeque<Vec<u8>>,
    pm_calls: VecDeque<Vec<u8>>,
    sos_calls: VecDeque<Vec<u8>>,
    apc_calls: VecDeque<Vec<u8>>,
    print_calls: VecDeque<Vec<u8>>,
    exec_calls: VecDeque<u8>,
}

impl TestWriter {
    pub fn new() -> Self {
        TestWriter {
            ansi_calls: VecDeque::new(),
            dec_calls: VecDeque::new(),
            dcs_calls: VecDeque::new(),
            esc_calls: VecDeque::new(),
            osc_calls: VecDeque::new(),
            pm_calls: VecDeque::new(),
            sos_calls: VecDeque::new(),
            apc_calls: VecDeque::new(),
            print_calls: VecDeque::new(),
            exec_calls: VecDeque::new(),
        }
    }

    pub fn get_ansi_call(&mut self) -> Option<(Vec<String>, String, char)> {
        self.ansi_calls.pop_front()
    }

    pub fn assert_ansi_call(
        &mut self,
        expected_parameters: &[&str],
        expected_intermediates: &str,
        expected_final_char: char,
    ) {
        if let Some((parameters, intermediates, final_char)) = self.get_ansi_call() {
            assert_eq!(parameters, expected_parameters);
            assert_eq!(intermediates, expected_intermediates);
            assert_eq!(final_char, expected_final_char);
        } else {
            panic!("No ANSI call found!");
        }
    }

    pub fn get_dec_call(&mut self) -> Option<(Vec<String>, String, char)> {
        self.dec_calls.pop_front()
    }

    pub fn assert_dec_call(
        &mut self,
        expected_parameters: &[&str],
        expected_intermediates: &str,
        expected_final_char: char,
    ) {
        if let Some((parameters, intermediates, final_char)) = self.get_dec_call() {
            assert_eq!(parameters, expected_parameters);
            assert_eq!(intermediates, expected_intermediates);
            assert_eq!(final_char, expected_final_char);
        } else {
            panic!("No DEC call found!");
        }
    }

    pub fn get_esc_call(&mut self) -> Option<(String, char)> {
        self.esc_calls.pop_front()
    }

    pub fn assert_esc_call(&mut self, expected_intermediates: &str, expected_final_char: char) {
        if let Some((intermediates, final_char)) = self.get_esc_call() {
            assert_eq!(intermediates, expected_intermediates);
            assert_eq!(final_char, expected_final_char);
        } else {
            panic!("No ESC call found!");
        }
    }

    pub fn get_osc_call(&mut self) -> Option<Vec<u8>> {
        self.osc_calls.pop_front()
    }

    pub fn assert_osc_call(&mut self, expected_data_string: &[u8]) {
        if let Some(data_string) = self.get_osc_call() {
            assert_eq!(data_string, expected_data_string);
        } else {
            panic!("No OSC call found!");
        }
    }

    fn get_pm_call(&mut self) -> Option<Vec<u8>> {
        self.pm_calls.pop_front()
    }

    pub fn assert_pm_call(&mut self, expected_data_string: &[u8]) {
        if let Some(data_string) = self.get_pm_call() {
            assert_eq!(data_string, expected_data_string);
        } else {
            panic!("No PM call found!");
        }
    }

    fn get_apc_call(&mut self) -> Option<Vec<u8>> {
        self.apc_calls.pop_front()
    }

    pub fn assert_apc_call(&mut self, expected_data_string: &[u8]) {
        if let Some(data_string) = self.get_apc_call() {
            assert_eq!(data_string, expected_data_string);
        } else {
            panic!("No APC call found!");
        }
    }

    fn get_print_call(&mut self) -> Option<Vec<u8>> {
        self.print_calls.pop_front()
    }

    pub fn assert_print_call(&mut self, expected_value: &[u8]) {
        if let Some(value) = self.get_print_call() {
            assert_eq!(value, expected_value);
        } else {
            panic!("No print call found!");
        }
    }

    fn get_exec_call(&mut self) -> Option<u8> {
        self.exec_calls.pop_front()
    }

    pub fn assert_exec_call(&mut self, expected_function: u8) {
        if let Some(function) = self.get_exec_call() {
            assert_eq!(function, expected_function);
        } else {
            panic!("No exec call found!");
        }
    }
}

impl MessageWriter for TestWriter {
    type Error = ();

    fn write_ansi(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        self.ansi_calls.push_back((
            parameters.iter().map(|x| x.to_string()).collect(),
            intermediates.to_string(),
            final_char,
        ));
        Ok(())
    }

    fn write_dec(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
    ) -> Result<(), Self::Error> {
        self.dec_calls.push_back((
            parameters.iter().map(|x| x.to_string()).collect(),
            intermediates.to_string(),
            final_char,
        ));
        Ok(())
    }

    fn write_dcs(
        &mut self,
        parameters: &[&str],
        intermediates: &str,
        final_char: char,
        data_string: &[u8],
    ) -> Result<(), Self::Error> {
        self.dcs_calls.push_back((
            parameters.iter().map(|x| x.to_string()).collect(),
            intermediates.to_string(),
            final_char,
            data_string.to_vec(),
        ));
        Ok(())
    }

    fn write_esc(&mut self, intermediates: &str, final_char: char) -> Result<(), Self::Error> {
        self.esc_calls
            .push_back((intermediates.to_string(), final_char));
        Ok(())
    }

    fn write_osc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.osc_calls.push_back(data_string.to_vec());
        Ok(())
    }

    fn write_pm(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.pm_calls.push_back(data_string.to_vec());
        Ok(())
    }

    fn write_sos(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.sos_calls.push_back(data_string.to_vec());
        Ok(())
    }

    fn write_apc(&mut self, data_string: &[u8]) -> Result<(), Self::Error> {
        self.apc_calls.push_back(data_string.to_vec());
        Ok(())
    }

    fn write_print(&mut self, value: &[u8]) -> Result<(), Self::Error> {
        self.print_calls.push_back(value.to_vec());
        Ok(())
    }

    fn write_exec(&mut self, function: u8) -> Result<(), Self::Error> {
        self.exec_calls.push_back(function);
        Ok(())
    }
}
