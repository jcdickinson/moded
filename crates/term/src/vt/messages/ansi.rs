use crate::{util::IntoNumber, vt::Version};

use super::{Message, MessageFilter};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PrimaryDeviceAttributesRequest;

impl PrimaryDeviceAttributesRequest {
    pub fn new() -> Self {
        Self
    }
}

impl Message for PrimaryDeviceAttributesRequest {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::ansi(|f| {
            f.with_param_count(1..=1)
                .with_first_param("0")
                .with_final('c')
        })]
    }

    fn try_parse_ansi(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "" && final_char == 'c' {
            match parameters {
                ["0"] => Some(Self),
                _ => None,
            }
        } else {
            None
        }
    }

    fn write_to<W: super::MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        writer.write_ansi(&["0"], "", 'c')
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SelectConformanceLevel {
    version: Version,
    is_8bit: bool,
}

impl SelectConformanceLevel {
    pub fn new(version: Version, is_8bit: bool) -> Self {
        Self { version, is_8bit }
    }
}

impl Message for SelectConformanceLevel {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::ansi(|c| {
            c.with_param_count(1..=2).with_final('p')
        })]
    }

    fn try_parse_ansi(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "" && final_char == 'p' {
            match (
                parameters
                    .get(0)
                    .and_then(|v| v.parse::<u16>().ok())
                    .map(Version::from_relative_value),
                parameters.get(1).and_then(|v| v.parse().ok()),
            ) {
                (Some(Version::Vt100), _) => Some(Self {
                    version: Version::Vt100,
                    is_8bit: false,
                }),
                (Some(version), Some(0u16 | 2)) => Some(Self {
                    version,
                    is_8bit: true,
                }),
                (Some(version), Some(1)) => Some(Self {
                    version,
                    is_8bit: false,
                }),
                (Some(version), None) if version >= Version::Vt220 => Some(Self {
                    version,
                    is_8bit: false,
                }),
                (Some(version), _) => Some(Self {
                    version,
                    is_8bit: false,
                }),
                _ => None,
            }
        } else {
            None
        }
    }

    fn write_to<W: super::MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        let val = if self.is_8bit { "2" } else { "0" };
        let parameters = &[self.version.relative_value().into_number(), val];
        writer.write_ansi(parameters, "", 'p')
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct OperatingStatusRequest;

impl OperatingStatusRequest {
    pub fn new() -> Self {
        Self
    }
}

impl Message for OperatingStatusRequest {
    fn create_filters() -> Vec<MessageFilter> {
        vec![MessageFilter::ansi(|f| {
            f.with_param_count(1..=1).with_final('n')
        })]
    }

    fn try_parse_ansi(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "" && final_char == 'n' {
            match parameters {
                ["5"] => Some(Self),
                _ => None,
            }
        } else {
            None
        }
    }

    fn write_to<W: super::MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        writer.write_ansi(&["5"], "", 'n')
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct OperatingStatusResponse {
    healthy: bool,
}

impl OperatingStatusResponse {
    pub fn new(healthy: bool) -> Self {
        Self { healthy }
    }
}

impl Message for OperatingStatusResponse {
    fn create_filters() -> Vec<MessageFilter> {
        vec![
            MessageFilter::ansi(|f| {
                f.with_param_count(1..=1)
                    .with_first_param("0")
                    .with_final('n')
            }),
            MessageFilter::ansi(|f| {
                f.with_param_count(1..=1)
                    .with_first_param("3")
                    .with_final('n')
            }),
        ]
    }

    fn try_parse_ansi(parameters: &[&str], intermediates: &str, final_char: char) -> Option<Self> {
        if intermediates == "" && final_char == 'n' {
            match parameters {
                ["0"] => Some(Self { healthy: true }),
                ["3"] => Some(Self { healthy: false }),
                _ => None,
            }
        } else {
            None
        }
    }

    fn write_to<W: super::MessageWriter>(&self, writer: &mut W) -> Result<(), W::Error> {
        if self.healthy {
            writer.write_ansi(&["0"], "", 'n')
        } else {
            writer.write_ansi(&["3"], "", 'n')
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::vt::messages::test_writer::TestWriter;

    use super::*;

    #[test]
    fn operating_status_response_try_parse_ansi() {
        let healthy = OperatingStatusResponse { healthy: true };
        let not_healthy = OperatingStatusResponse { healthy: false };

        assert_eq!(
            OperatingStatusResponse::try_parse_ansi(&["0"], "", 'n'),
            Some(healthy)
        );

        assert_eq!(
            OperatingStatusResponse::try_parse_ansi(&["3"], "", 'n'),
            Some(not_healthy)
        );

        assert_eq!(
            OperatingStatusResponse::try_parse_ansi(&["4"], "", 'n'),
            None
        );
    }

    #[test]
    fn operating_status_response_write_to() {
        let mut buffer = TestWriter::new();
        let healthy = OperatingStatusResponse { healthy: true };
        let not_healthy = OperatingStatusResponse { healthy: false };

        healthy.write_to(&mut buffer).unwrap();
        buffer.assert_ansi_call(&["0"], "", 'n');

        not_healthy.write_to(&mut buffer).unwrap();
        buffer.assert_ansi_call(&["3"], "", 'n');
    }

    #[test]
    fn select_conformance_level_new() {
        let level = SelectConformanceLevel::new(Version::Vt220, true);
        assert_eq!(level.version, Version::Vt220);
        assert_eq!(level.is_8bit, true);
    }

    #[test]
    fn select_conformance_level_create_filters() {
        let filters = SelectConformanceLevel::create_filters();
        assert_eq!(filters.len(), 1);
    }

    #[test]
    fn select_conformance_level_try_parse_ansi() {
        let level1 = SelectConformanceLevel::try_parse_ansi(&["62"], "", 'p').unwrap();
        assert_eq!(level1.version, Version::Vt220);
        assert_eq!(level1.is_8bit, false);

        let level2 = SelectConformanceLevel::try_parse_ansi(&["62", "1"], "", 'p').unwrap();
        assert_eq!(level2.version, Version::Vt220);
        assert_eq!(level2.is_8bit, false);

        let level3 = SelectConformanceLevel::try_parse_ansi(&["61"], "", 'p').unwrap();
        assert_eq!(level3.version, Version::Vt100);
        assert_eq!(level3.is_8bit, false);

        let level4 = SelectConformanceLevel::try_parse_ansi(&["61", "0"], "", 'p').unwrap();
        assert_eq!(level4.version, Version::Vt100);
        assert_eq!(level4.is_8bit, false);

        let level5 = SelectConformanceLevel::try_parse_ansi(&["61", "2"], "", 'p').unwrap();
        assert_eq!(level5.version, Version::Vt100);
        assert_eq!(level5.is_8bit, false);

        let level6 = SelectConformanceLevel::try_parse_ansi(&["62", "3"], "", 'p').unwrap();
        assert_eq!(level6.version, Version::Vt220);
        assert_eq!(level6.is_8bit, false);

        let level6 = SelectConformanceLevel::try_parse_ansi(&["63", "0"], "", 'p').unwrap();
        assert_eq!(level6.version, Version::Vt320);
        assert_eq!(level6.is_8bit, true);

        let level6 = SelectConformanceLevel::try_parse_ansi(&["63", "2"], "", 'p').unwrap();
        assert_eq!(level6.version, Version::Vt320);
        assert_eq!(level6.is_8bit, true);
    }

    #[test]
    fn select_conformance_level_write_to() {
        let level = SelectConformanceLevel::new(Version::Vt220, true);
        let mut writer = TestWriter::new();
        level.write_to(&mut writer).unwrap();

        writer.assert_ansi_call(&["62", "2"], "", 'p');
    }

    #[test]
    fn operating_status_request_try_parse_ansi_valid() {
        let result = OperatingStatusRequest::try_parse_ansi(&["5"], "", 'n');
        assert_eq!(result, Some(OperatingStatusRequest));
    }

    #[test]
    fn operating_status_request_try_parse_ansi_invalid() {
        let result = OperatingStatusRequest::try_parse_ansi(&["4"], "", 'n');
        assert_eq!(result, None);
    }

    #[test]
    fn operating_status_request_write_to() {
        let mut writer = TestWriter::new();
        let request = OperatingStatusRequest::new();
        request.write_to(&mut writer).unwrap();
        writer.assert_ansi_call(&["5"], "", 'n');
    }

    #[test]
    fn primary_device_attributes_try_parse_ansi() {
        // valid input
        let expected = Some(PrimaryDeviceAttributesRequest::new());
        let result = PrimaryDeviceAttributesRequest::try_parse_ansi(&["0"], "", 'c');
        assert_eq!(result, expected);

        // invalid input
        let expected = None;
        let result = PrimaryDeviceAttributesRequest::try_parse_ansi(&["1"], "", 'c');
        assert_eq!(result, expected);
    }

    #[test]
    fn primary_device_attributes_write_to() {
        let mut writer = TestWriter::new();
        let message = PrimaryDeviceAttributesRequest::new();
        message.write_to(&mut writer).unwrap();
        writer.assert_ansi_call(&["0"], "", 'c');
    }
}
