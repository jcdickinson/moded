mod messages;
mod parser;
mod scanner;

use std::cmp::Ordering;

pub use messages::*;
use num_enum::{FromPrimitive, IntoPrimitive};
pub use parser::{Parser, ParserSink};
pub use scanner::{Scanner, ScannerError, ScannerSink, Token};

#[derive(Debug, Clone, Copy, PartialEq, Hash, FromPrimitive, IntoPrimitive, Eq)]
#[repr(u16)]
pub enum Version {
    Vt100 = 1,
    Vt220 = 62,
    Vt320 = 63,
    Vt420 = 64,
    #[num_enum(catch_all)]
    Unknown(u16),
}

impl Version {
    pub fn relative_value(&self) -> u16 {
        match self {
            Version::Vt100 => 61,
            Version::Vt220 => 62,
            Version::Vt320 => 63,
            Version::Vt420 => 64,
            Version::Unknown(v) => *v,
        }
    }

    pub fn from_relative_value(value: u16) -> Self {
        match value {
            61 => Version::Vt100,
            62 => Version::Vt220,
            63 => Version::Vt320,
            64 => Version::Vt420,
            value => Version::Unknown(value),
        }
    }
}

impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.relative_value().partial_cmp(&other.relative_value())
    }
}

macro_rules! term_cap_names {
    ($($name: ident => $hex: literal),+) => {
        #[derive(Clone, PartialEq, Eq, Hash)]
        pub enum TermCapName {
            $($name),+,
            Unknown(Vec<u8>)
        }

        impl AsRef<[u8]> for TermCapName {
            fn as_ref(&self) -> &[u8] {
                match self {
                    $( Self::$name => $hex ),+,
                    Self::Unknown(v) => &v
                }
            }
        }

        impl From<&[u8]> for TermCapName {
            fn from(value: &[u8]) -> TermCapName {
                match value {
                    $( $hex => Self::$name ),+,
                    unknown => Self::Unknown(unknown.to_vec())
                }
            }
        }
    };
}

term_cap_names! {
    TerminalName => b"544E", // TN
    Colors => b"636F6C6F7273", // colors
    TrueColor => b"5463", // Tc
    StyledUnderlines => b"5375" // Su
}
