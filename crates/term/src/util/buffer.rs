use std::{
    fmt::Debug,
    io::Read,
    ops::{Deref, Range},
};

use super::PrintableSeq;

const DEFAULT_SIZE: usize = 512;
const GROWTH_NUM: usize = 3;
const GROWTH_DEN: usize = 2;

pub struct Buffer {
    data: Box<[u8]>,
    available: Range<usize>,
    consumed: usize,
}

impl Debug for Buffer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Buffer").field(&PrintableSeq(self)).finish()
    }
}

impl Default for Buffer {
    fn default() -> Self {
        Self {
            data: vec![0u8; DEFAULT_SIZE].into_boxed_slice(),
            available: 0..0,
            consumed: 0,
        }
    }
}

impl Deref for Buffer {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}

impl AsRef<[u8]> for Buffer {
    fn as_ref(&self) -> &[u8] {
        self.as_slice()
    }
}

impl Buffer {
    pub fn from_slice(value: impl AsRef<[u8]>) -> Self {
        let value = value.as_ref();
        let len = value.len();
        Buffer {
            data: value.to_vec().into_boxed_slice(),
            available: 0..len,
            consumed: 0,
        }
    }

    pub fn read_from<R>(&mut self, r: &mut R) -> std::io::Result<usize>
    where
        R: Read + ?Sized,
    {
        debug_assert_ne!(self.data.len() - self.available.end, 0);
        let l = r.read(&mut self.data[self.available.end..])?;
        self.available = self.available.start..(self.available.end + l);
        Ok(l)
    }

    #[inline]
    pub fn as_slice(&self) -> &[u8] {
        &self.data[self.available.clone()]
    }

    #[cfg(test)]
    fn check_size(&self, _: usize) {}

    #[cfg(not(test))]
    fn check_size(&self, size: usize) {
        debug_assert!(size <= self.available.len());
    }

    pub fn consume_with<R>(&mut self, size: usize, mut f: impl FnMut(&[u8]) -> R) -> R {
        let size = size.min(self.available.len());

        let result = f(&self[..size]);
        self.consume(size);
        result
    }

    pub fn consume(&mut self, size: usize) -> usize {
        if size == 0 {
            return self.available.len();
        }

        self.check_size(size);
        let size = size.min(self.available.len());
        self.consumed += size;
        self.available.start += size;
        let l = self.available.len();
        if l == 0 {
            self.available = 0..0;
        }
        l
    }

    #[cfg_attr(coverage_nightly, no_coverage)]
    pub fn must_advance(&mut self, token: &mut Option<usize>) -> Option<bool> {
        if self.len() == 0 {
            return Some(false);
        }

        if let Some(token_value) = token.take() {
            if token_value == self.consumed {
                return None;
            }
        }

        *token = Some(self.consumed);
        Some(true)
    }

    pub fn ensure_capacity(&mut self, capacity: usize) {
        let required = self.available.end + capacity;
        if required <= self.data.len() {
            return;
        }

        self.data.copy_within(self.available.clone(), 0);
        self.available = 0..(self.available.len());

        let required = self.available.end + capacity;
        let mut fin = self.data.len();
        while required > fin {
            fin = (fin * GROWTH_NUM) / GROWTH_DEN;
        }
        self.grow(fin);
    }

    fn grow(&mut self, to: usize) {
        if to <= self.data.len() {
            return;
        }

        let mut new = vec![0u8; to].into_boxed_slice();
        new[0..self.available.len()].copy_from_slice(&self.data[self.available.clone()]);

        self.available = 0..(self.available.len());
        self.data = new;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn growth_retains_data() {
        let mut sut = Buffer::default();
        for i in 0..sut.data.len() {
            sut.data[i] = ((i * i) & 0xFF) as u8;
        }

        sut.ensure_capacity(DEFAULT_SIZE + 1);

        assert_eq!(sut.data.len(), (DEFAULT_SIZE * GROWTH_NUM) / GROWTH_DEN);

        for i in 0..sut.data.len() {
            assert_eq!(sut.data[i], 0);
        }
    }

    #[test]
    fn growth_copies_available() {
        let mut sut = Buffer::default();
        for i in 0..sut.data.len() {
            sut.data[i] = ((i * i) & 0xFF) as u8;
        }
        sut.available = 50..100;
        sut.ensure_capacity(DEFAULT_SIZE + 1);

        for i in 0..50 {
            let j = i + 50;
            assert_eq!(sut.data[i], ((j * j) & 0xFF) as u8);
        }
    }

    #[test]
    fn no_growth_if_large_enough() {
        let mut sut = Buffer::default();
        sut.ensure_capacity(10);
        assert_eq!(sut.data.len(), DEFAULT_SIZE);
    }

    #[test]
    fn growth_with_bytes_available() {
        let mut sut = Buffer::default();
        sut.available = 50..100;
        sut.ensure_capacity(DEFAULT_SIZE - 1);
        assert_eq!(sut.data.len(), (DEFAULT_SIZE * GROWTH_NUM) / GROWTH_DEN);
    }

    #[test]
    fn consume_deletes_bytes() {
        let mut sut = Buffer::default();
        sut.available = 50..100;
        sut.consume(10);
        assert_eq!(sut.available, 60..100);
    }

    #[test]
    fn consume_capped() {
        let mut sut = Buffer::default();
        sut.available = 50..100;
        sut.consume(60);
        assert_eq!(sut.available.len(), 0);
    }

    #[test]
    fn read_from() {
        let mut sut = Buffer::default();
        assert_eq!(sut.read_from(&mut b"test".as_slice()).unwrap(), 4);
        sut.consume(3);
        assert_eq!(sut.read_from(&mut b"123".as_slice()).unwrap(), 3);
        assert_eq!(sut.deref(), b"t123");
    }
}
