#![cfg_attr(coverage_nightly, feature(no_coverage))]

#[macro_use]
pub(crate) mod util;

pub mod tty;

/// VT Parser
///
/// Provides a parser that loosely conforms to
/// [Paul Flo Williams' VT100 State Machine]. Several adjustments have been
/// made to ensure that it covers the entire V100 spec, instead of documented
/// function. This means that it should be able to handle any user-defined
/// extensions to the protocol.
///
/// [Paul Flo Williams' VT100 State Machine]: https://vt100.net/emu/dec_ansi_parser
pub mod vt;

pub type PlatformTerminal = tty::Tty;
pub trait Terminal: Sized {
    type Renderer<'a>: TerminalRenderer
    where
        Self: 'a;
    type Error: std::error::Error;

    fn create_default() -> Result<Self, Self::Error>;

    fn render<'a, R>(
        &'a mut self,
        renderer: impl FnOnce(&mut Self::Renderer<'a>) -> R,
    ) -> Result<R, Self::Error>;
}

pub trait TerminalRenderer: Sized {
    type Error: std::error::Error;
    fn print(&mut self, text: &str) -> Result<(), Self::Error>;
    fn set_style(&mut self, fg: u16) -> Result<(), Self::Error>;
    fn reset_style(&mut self) -> Result<(), Self::Error>;
}
