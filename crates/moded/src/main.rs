use moded_term::{PlatformTerminal, Terminal, TerminalRenderer};

fn main() {
    let mut term = PlatformTerminal::create_default().unwrap();

    // let mut file = File::open("/dev/tty").unwrap();
    // let mut poll = Poll::new().unwrap();
    // let mut events = Events::with_capacity(1024);
    //
    // poll.registry()
    //     .register(
    //         &mut SourceFd(&file.as_raw_fd()),
    //         Token(0),
    //         Interest::READABLE | Interest::WRITABLE,
    //     )
    //     .unwrap();
    //
    // loop {
    //     poll.poll(&mut events, None).unwrap();
    //     for event in &events {
    //         if event.token() == Token(0) {
    //             if event.is_readable() {
    //                 println!("r {}", event.is_readable());
    //             }
    //         }
    //     }
    // }
    //
    term.render(|r| -> Result<(), std::io::Error> {
        r.set_style(31)?;
        r.print("hello world")?;
        r.reset_style()?;
        Ok(())
    })
    .unwrap()
    .unwrap();
    std::thread::sleep(std::time::Duration::from_secs(5));
}
